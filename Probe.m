clearvars

stargate_path = 'C:\Users\F.Kunkler\Desktop\VM\stargate';
% stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';

VOC =["BENZENE"];
load(fullfile(stargate_path,'output_matlab',strcat('output_all_',(VOC),'.mat')));



for k=1:length(VM_all.output_struct.after)
sum_product_benz(k)=(VM_all.output_struct.after(k).PHENOL)+(VM_all.output_struct.after(k).BZBIPERO2)+(VM_all.output_struct.after(k).BZEPOXMUC);
end
sum_product_benz=sum_product_benz.';
OH_without_VOC = VM_all.output_struct.after(1).OH;
proportion_rest_OH=([VM_all.output_struct.after.OH]/OH_without_VOC*100).';

products_vs_OH=(sum_product_benz/OH_without_VOC).';