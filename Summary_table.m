clearvars
stargate_path = 'C:\Users\F.Kunkler\Desktop\VM\stargate';
VOC = ["C5H8"];

load(fullfile(stargate_path,'output_matlab',strcat('output_all_',VOC,'.mat')));
save_path = fullfile('C:\Users\F.Kunkler\Desktop\VM\','Figures',VOC);

switch VOC
    case "CH4"
        RRG_VOC = ["RRG4101","RRG3200","RRG3201"]; %CH4 + OH = CH3 + H2O , HONO ,HO2+NO
        RO2 = ["CH3O2"];
        side_products = ["CH3OOH","HCHO"];
        intern_RO2_product = ["HCHO"];   % Endproduct of CH3O2
        intern_RO2_side =["CH3ONO","CH3O"]; % sideproducts of CH3O2 + NO and further
    case "C2H4"
        RRG_VOC = ["RRG42002","RRG3200","RRG3201"];
        RO2 = ["HOCH2CH2O2"];
        side_products = ["HOCH2CH2O","HOCH2CHO","ETHGLY","HYETHO2H"];
        intern_RO2_product = ["HOCH2CHO","HCHO"];   % HCHO is the main Product  HOCH2CH2O2 + NO   = .25 HO2 + .5 HCHO + .75 HOCH2CH2O + NO2  !4 Größenordnungen höher als andere Reaktionen von RO2!
        intern_RO2_side =["HOCH2CH2O","ETHOHNO3"]; % sideproducts 
    case "BENZENE"
        RRG_VOC = ["RRG46441","RRG3200","RRG3201"];
        RO2 = ["BZBIPERO2"]; % Reagiert mit NO BZBIPERO2  + NO     = NO2 + GLYOX + HO2 + .5 BZFUONE + .5 BZFUONE  2 Größenordnungen schneller als andere reaktionen von RO2
        side_products = ["PHENOL","BZEPOXMUC","CATECHOL","BZEMUCO2","BZEMUCCO3"];
        intern_RO2_product = ["GLYOX","BZFUONE"];   % 
        intern_RO2_side =["BZBIPERNO3"]; % 
    case "C5H8"
        RO2 = ["LISOPACO2","ISOPBO2","LDISOPACO2","ISOPDO2","LISOPEFO2"];
        side_products = ["LISOPAB","LISOPCD","C1ODC2O2C4OOH","C1OOHC3O2C4OD","LZCODC23DBCOOH","ISOPAOH","LISOPACOOH"];
    case "C3H6"
        RRG_VOC = ["RRG43002","RRG3200","RRG3201"];
        RO2 = ["HYPROPO2"];
        side_products = ["HYPROPO2H","CH3CHO","ACETOL"];  
        intern_RO2_product = ["CH3CHO","HCHO"];   % 
        intern_RO2_side =["PROPOLNO3","HYPROPO2H","ACETOL"]; % 
    case "C3H8"
        RRG_VOC = ["RRG43000a","RRG43000b","RRG3200","RRG3201"];
        RO2 = ["IC3H7O2","NC3H7O2"];
        side_products = ["NC3H7OOH","IC3H7OOH"];  
        intern_RO2_product = ["C2H5CHO","CH3COCH3","HCHO"];   % 
        intern_RO2_side =["NC3H7NO3","IC3H7NO3","CH3COCH2O2","CH3CO","IPROPOL","HYPERACET","NOA","C2H5CO3"]; % Preproducts..
end


%% Summary_under_after 
% here we summarize if the OH (from the Lamp) Conversion is quantitativ
% furthermore we compare the formation of the mainproduct of the VOC with OH(or
% HO2) with the main-sideproducts
Summary_under_after = table;SP=table;MP=table;
var_names= [VOC,"OH",RO2,"HO2"];

for k=1:length(var_names)
    if (var_names(k))==VOC
        VOC_mr =[0];
       for l=2:length(fields(VM_all.runs.under))
        if l<10
            run = strcat('run000',num2str(l));
        elseif l>=10
            run = strcat('run00',num2str(l));
        elseif l>=100
            run = strcat('run0',num2str(l));
        end
        mr = VM_all.runs.under.(run)(1).(VOC);
        VOC_mr = [VOC_mr,mr];
       end
        Summary_under_after = addvars(Summary_under_after,([VOC_mr].'));
    else
        Summary_under_after = addvars(Summary_under_after,([VM_all.output_struct.after.(var_names(k))].'));
    end
end
Summary_under_after.Properties.VariableNames = var_names;
for k=1:length(RO2)
    MP = addvars(MP,([VM_all.output_struct.after.(RO2(k))].'));
end
MP.Properties.VariableNames = RO2;
MP.SumMP = sum(table2array(MP),2);
for k=1:length(side_products)
    SP = addvars(SP,([VM_all.output_struct.after.(side_products(k))].'));
end
SP.Properties.VariableNames = side_products;

SP.SumSP= sum(table2array(SP),2);
SP.Sum_SPandRO2 = ([SP.SumSP]+[MP.SumMP]);

Summary_under_after = [Summary_under_after SP];

%% Calcualtion 

OH_conversion = ((1-([VM_all.output_struct.after.OH]./VM_all.output_struct.after(1).OH))*100).';
Summary_under_after = addvars (Summary_under_after,OH_conversion,'NewVariableNames','OH_conv %');

if length (RO2)==1
    RO2_conversion = ((([VM_all.output_struct.after.(RO2)]./VM_all.output_struct.after(1).OH))*100).';
else
    fieldnames_output = fieldnames(VM_all.output_struct.after);
    fieldnames_RO2 = RO2.';
    Diff = setdiff(fieldnames_output,fieldnames_RO2);
    output_RO2 = removevars(struct2table(VM_all.output_struct.after),Diff);
    RO2_sum = sum(table2array(output_RO2),2);

    RO2_conversion = ((([RO2_sum.']./VM_all.output_struct.after(1).OH))*100).';
    Summary_under_after = addvars (Summary_under_after,RO2_sum,'NewVariableNames','RO2_sum');
end

Summary_under_after = addvars (Summary_under_after,RO2_conversion,'NewVariableNames','RO2_conv %');
Summary_under_after = addvars(Summary_under_after,((Summary_under_after.Sum_SPandRO2./Summary_under_after.OH(1))*100),'NewVariableNames','OH_to_any %');
Summary_under_after = addvars(Summary_under_after,((MP.SumMP./Summary_under_after.Sum_SPandRO2)*100),'NewVariableNames','MP vs MP+SP');


writetable(Summary_under_after,fullfile(save_path,strcat('Summary_under_after_',VOC,'.xlsx')))




%% Summary for intern
%Here we look into the conversion of HO2 and RO2 with NO to get OH
% Main focus is if out Peroxide is reduced nearly completly, same for HO2
% AND we need to look into Reaktion RRG4101 if the Reaction VOC + OH is
% happening to often

Summary_intern_date = table;
intern_Variables = ["NO","OH",RRG_VOC,"HO2",RO2,intern_RO2_product,intern_RO2_side];

for k=1:length(intern_Variables)
    if any(contains(RRG_VOC,intern_Variables(k))) == true
        Summary_intern_date= addvars(Summary_intern_date,[VM_all.sum_RR_output_struct.intern.(intern_Variables(k))].');
    else
        Summary_intern_date= addvars(Summary_intern_date,[VM_all.output_struct.intern.(intern_Variables(k))].');
    end
end
Summary_intern_date.Properties.VariableNames = intern_Variables;

if any (contains (RRG_VOC,"RRG3200"))==true
    Summary_intern_date.Properties.VariableNames{'RRG3200'} = 'RR NO+OH=HONO';
end
if any (contains (RRG_VOC,"RRG3201"))==true
    Summary_intern_date.Properties.VariableNames{'RRG3201'} = 'NO+ HO2=NO2+OH';
end



%% Plots under/after

plot(Summary_under_after.(VOC)(2:end),Summary_under_after.("OH_conv %")(2:end),'marker','.','MarkerSize',15)
grid on; hold on;
title(strcat("OH conversion for ",(VOC)))
xlabel (strcat((VOC), "[mr]")); ylabel("OH conversion [%]");
hold off;













%% Calculate Summary interns
ZP=table;MP=table;Sum_RO2=table;
% RO2 Reduction
for k=1:length(RO2)
    Sum_RO2 = addvars(Sum_RO2,([VM_all.output_struct.intern.(RO2(k))].'));
end
Sum_RO2.Properties.VariableNames = RO2;
Sum_RO2.SumRO2 = sum(table2array(Sum_RO2),2);

RO2_red = Sum_RO2.SumRO2-Sum_RO2.SumRO2(1);
RO2_red_perc = (1-(Sum_RO2.SumRO2/Sum_RO2.SumRO2(1)))*100;
Summary_intern_date = addvars (Summary_intern_date,RO2_red,'NewVariableNames','RO2_red');
Summary_intern_date = addvars (Summary_intern_date,RO2_red_perc,'NewVariableNames','RO2_red%');


% Main Product 
for k=1:length(intern_RO2_product)
    MP = addvars(MP,([VM_all.output_struct.intern.(intern_RO2_product(k))].'));
end
MP.Properties.VariableNames = intern_RO2_product;
MP.SumMP = sum(table2array(MP),2);

%Side Products
for k=1:length(intern_RO2_side)
    ZP = addvars(ZP,([VM_all.output_struct.intern.(intern_RO2_side(k))].'));
end
ZP.Properties.VariableNames = intern_RO2_side;
ZP.SumZP = sum(table2array(ZP),2);

Summary_intern_date = addvars (Summary_intern_date,(MP.SumMP+ZP.SumZP),'NewVariableNames','Sum_Products');

A=[intern_RO2_product, intern_RO2_side];
for k=1:length(A)
    Summary_intern_date = addvars (Summary_intern_date,((Summary_intern_date.(A(k))./Summary_intern_date.Sum_Products)*100)...
        ,'NewVariableNames',strcat(A(k),"% in Products"));
end


writetable(Summary_intern_date,fullfile(save_path,strcat('Summary_intern_',VOC,'.xlsx'))) 
