function [output_mat,output_struct] = readin_model_output(stargate_path,filename,filter_variable,output_path,input_var)
% function to read in and save caaba multirun output from given path
% Script from Sebastian, modified by Roland, lightly changed by Felix
if nargin < 5
    input_var={'Tgps';'nml_press';'nml_temp'};
end

if nargin < 4
    output_path=fullfile(stargate_path,'output_matlab',strcat("output_",filename,".mat"));
end

if nargin < 3
    filter_variable=true;
end
%% find all runs
runs_path=fullfile(stargate_path,filename,'\runs\');
d=dir2(runs_path);
%% read in single runs

l=length(d);
current_tab     =   nc2table(fullfile(runs_path,d(l).name,'\caaba_mecca_c_end.nc')) ;  
output_structR(length(d),:)=current_tab;    % As a workaround for table pre-allocation that is tricky, I just initialize the last field 
    
for l=1:length(d)    
    current_tab     =   nc2table( fullfile(runs_path,d(l).name,'\caaba_mecca_c_end.nc') );
    output_structR(l,:)=    current_tab;
end

%% get timestamp, pressure and temperature from input file
input_path=fullfile(stargate_path,filename);
all_input	=   nc2table( fullfile(input_path,'input.nc') );

for k=1:length(input_var)
    varname_input=['input_',char(input_var(k))];
    output_struct_input(:,k)=table(all_input.(input_var{k}));
end
output_struct_input.Properties.VariableNames=input_var';

output_struct=[output_struct_input,output_structR];
%% filter variables that dont change
if filter_variable==true
    index_diff=[];
    for k=4:length(fieldnames(output_struct))-3
        % FK: bei if Anstelle von : length( unique( table2array(output_struct(:,k))))==1 jetzt sum(table2array(output_struct(:,k)))==0        
        if sum(table2array(output_struct(:,k)))==0% FK: Filtert Variablen die nur 0 bleiben raus. (CaabaMecca gibt eh keine negativen Werte aus)
            output_struct{:,k}=NaN;
            index_diff=[index_diff,k];
        end
    end
    output_struct(:,index_diff)=[];
end
output_struct=table2struct(output_struct);

output_mat=output_path;
save(output_path,'output_struct');


end