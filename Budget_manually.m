clearvars 

Config_at_userpath;

filename = 'IPI_CERN_11-Jan-2022_OH_4.8e-13';
% filename = 'CH4_50lmin_06-Oct-2020_after_lamp';

budget_for = ["OH","HO2"];
VOC = "C3H8";
%%
load(strcat(stargate_path,'\output_matlab\output_runs_',filename,'.mat'));
load (strcat(stargate_path,'\output_matlab\output_',filename,'.mat'));

budgets = struct;


a=struct2table(runs);


%% Choose Run and what u want 


c=height(struct2table(runs.run0001));
b=length(a.Properties.VariableNames(2));


for k=1:length(budget_for) 
    datfile_path(k)=strcat(budget_path,'\budgetUnKnwn',budget_for(k),'.dat');
end

%% Summs Up all fields for the Reaktionfields (RR)

special= ["RRJ2100a"];
for n=1:length(a.Properties.VariableNames)
    if n < 10
        for k=1000:10000
            if isfield ((runs.(strcat('run000',num2str(n)))),(strcat('RRG',num2str(k))))
                sum_field = sum([runs.(strcat('run000',num2str(n))).(strcat('RRG',num2str(k)))]); 
                for l=1:height(struct2table(runs.run0001))
                runs.(strcat('run000',num2str(n)))(l).(strcat('RRG',num2str(k))) = sum_field;
                end
            end
            if isfield ((runs.(strcat('run000',num2str(n)))),(strcat('RRG',num2str(k),'a')))
                sum_field = sum([runs.(strcat('run000',num2str(n))).(strcat('RRG',num2str(k),'a'))]); 
                for l=1:height(struct2table(runs.run0001))
                runs.(strcat('run000',num2str(n)))(l).(strcat('RRG',num2str(k))) = sum_field;
                end
            end
            if isfield ((runs.(strcat('run000',num2str(n)))),(strcat('RRG',num2str(k),'b')))
                sum_field = sum([runs.(strcat('run000',num2str(n))).(strcat('RRG',num2str(k),'b'))]); 
                for l=1:height(struct2table(runs.run0001))
                runs.(strcat('run000',num2str(n)))(l).(strcat('RRG',num2str(k))) = sum_field;
                end
            end
             if isfield ((runs.(strcat('run000',num2str(n)))),(strcat('RRG',num2str(k),'c')))
                sum_field = sum([runs.(strcat('run000',num2str(n))).(strcat('RRG',num2str(k),'a'))]); 
                for l=1:height(struct2table(runs.run0001))
                runs.(strcat('run000',num2str(n)))(l).(strcat('RRG',num2str(k))) = sum_field;
                end
            end
        end
        for m=1:length(special)
            if isfield ((runs.(strcat('run000',num2str(n)))),(special(m)))
                sum_field = sum([runs.(strcat('run000',num2str(n))).(special(m))]); 
                for l=1:height(struct2table(runs.run0001))
                runs.(strcat('run000',num2str(n)))(l).(special(m)) = sum_field;
                end
            end
        end
        
    elseif n>=10
                for k=1000:10000
            if isfield ((runs.(strcat('run00',num2str(n)))),(strcat('RRG',num2str(k))))
                sum_field = sum([runs.(strcat('run00',num2str(n))).(strcat('RRG',num2str(k)))]); 
                for l=1:height(struct2table(runs.run0001))
                runs.(strcat('run00',num2str(n)))(l).(strcat('RRG',num2str(k))) = sum_field;
                end
            end
            if isfield ((runs.(strcat('run00',num2str(n)))),(strcat('RRG',num2str(k),'a')))
                sum_field = sum([runs.(strcat('run00',num2str(n))).(strcat('RRG',num2str(k),'a'))]); 
                for l=1:height(struct2table(runs.run0001))
                runs.(strcat('run00',num2str(n)))(l).(strcat('RRG',num2str(k))) = sum_field;
                end
            end
            if isfield ((runs.(strcat('run00',num2str(n)))),(strcat('RRG',num2str(k),'b')))
                sum_field = sum([runs.(strcat('run00',num2str(n))).(strcat('RRG',num2str(k),'b'))]); 
                for l=1:height(struct2table(runs.run0001))
                runs.(strcat('run00',num2str(n)))(l).(strcat('RRG',num2str(k))) = sum_field;
                end
            end
             if isfield ((runs.(strcat('run00',num2str(n)))),(strcat('RRG',num2str(k),'c')))
                sum_field = sum([runs.(strcat('run00',num2str(n))).(strcat('RRG',num2str(k),'a'))]); 
                for l=1:height(struct2table(runs.run0001))
                runs.(strcat('run00',num2str(n)))(l).(strcat('RRG',num2str(k))) = sum_field;
                end
            end
        end
        for m=1:length(special)
            if isfield ((runs.(strcat('run00',num2str(n)))),(special(m)))
                sum_field = sum([runs.(strcat('run00',num2str(n))).(special(m))]); 
                for l=1:height(struct2table(runs.run0001))
                runs.(strcat('run00',num2str(n)))(l).(special(m)) = sum_field;
                end
            end
        end
    end

end
  






%% get Values for Budgets

for l=1:length(budget_for)
    for n=1:length(a.Properties.VariableNames)
        if n < 10
        for k=1:length(runs.(strcat('run000',num2str(n))))
            budgets.(budget_for(l)).(strcat('run000',num2str(n)))(k)= budgetcalc(datfile_path(l),runs.(strcat('run000',num2str(n)))(k));
        end
        elseif n >= 10
        for k=1:length(runs.(strcat('run00',num2str(n))))
            budgets.(budget_for(l)).(strcat('run00',num2str(n)))(k)= budgetcalc(datfile_path(l),runs.(strcat('run00',num2str(n)))(k));
        end
        end
    end
end


%% 
%in ProdRxn Reaktionsgleichungen der Produktion
VOCmr = [output_struct.(VOC)].'; %[] holt die VOC mr aus dem zugehörigen raus

%% in Loss all loss data per timestep same for prod
prod = struct; loss = struct;
for l=1:length(budget_for) 
    if budget_for(l)=="OH"
        if l==1 
            for k=1:length(VOCmr)
                if k == 1
                    prod0(:,(k)) = budgets.(budget_for(l))(k).Prod;
                    loss0(:,(k)) = budgets.(budget_for(l))(k).Loss;
                    else
                  
                    prod1(:,(k-1)) = budgets.(budget_for(l))(k).Prod;
                    loss1(:,(k-1)) = budgets.(budget_for(l))(k).Loss;      
                end
            end
                    prod0 = prod0.'; prod0(2,:)=repmat(0,[1,length(prod0)]);
                    prod1 = prod1.';
                    loss0 = loss0.'; loss0(2,:)=repmat(0,[1,length(loss0)]);
                    loss1 = loss1.';
                    prod.OHzeroVOC = prod0;prod.(budget_for(l)) = prod1;
                    loss.OHzeroVOC = loss0;loss.(budget_for(l)) = loss1;
                   
        elseif l==2 
            for k=1:length(VOCmr)
            prod2(:,(k)) = budgets.(budget_for(l))(k).Prod;
            loss2(:,(k)) = budgets.(budget_for(l))(k).Loss;   
            end
            prod2=prod2.'; prod.(budget_for(l)) = prod2;
            loss2 = loss2.';loss.(budget_for(l)) = loss2;
        
        elseif l==3 
         for k=1:length(VOCmr)
            prod3(:,(k-1)) = budgets.(budget_for(l))(k).Prod;
            loss3(:,(k-1)) = budgets.(budget_for(l))(k).Loss;   
         end
            prod3=prod3.'; prod.(budget_for(l)) = prod3;
            loss3 = loss3.';loss.(budget_for(l)) = loss3;
        end
    else  
         if l==1 
            for k=2:length(VOCmr)
            prod1(:,(k-1)) = budgets.(budget_for(l))(k).Prod;
            loss1(:,(k-1)) = budgets.(budget_for(l))(k).Loss;   
            end
            prod1 = prod1.';prod.(budget_for(l)) = prod1;
            loss1 = loss1.';loss.(budget_for(l)) = loss1;
        elseif l==2 
            for k=2:length(VOCmr)
            prod2(:,(k-1)) = budgets.(budget_for(l))(k).Prod;
            loss2(:,(k-1)) = budgets.(budget_for(l))(k).Loss;   
            end
            prod2=prod2.'; prod.(budget_for(l)) = prod2;
            loss2 = loss2.';loss.(budget_for(l)) = loss2;
        
        elseif l==3 
         for k=2:length(VOCmr)
            prod3(:,(k-1)) = budgets.(budget_for(l))(k).Prod;
            loss3(:,(k-1)) = budgets.(budget_for(l))(k).Loss;   
         end
            prod3=prod3.'; prod.(budget_for(l)) = prod3;
            loss3 = loss3.';loss.(budget_for(l)) = loss3;
         end
    end
end



%% Combines all other Production / Loss Channels so that we see only 3 channels








%% Plot of Loss and Prod


%if u want all 4 in 1

% figure;
% for l=1:length(budget_for)
% if l==1
%     subplot(4,1,l);
% elseif l==2
%     subplot(4,1,3)
% end
% title(strcat("Production of " , budget_for(l)))
% hold on; grid on;
% bar(VOCmr(2:end), prod.(budget_for(l)) ,'stacked')
% legend(budgets.(budget_for(l))(4).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
% legend('toggle')
% xlabel('time [s]');ylabel(strcat(budget_for(l), " [mr]"));
% subplot(4,1,2*l);
% hold on; grid on;
% title(strcat("Loss of " , budget_for(l)))
% bar(VOCmr(2:end), loss.(budget_for(l)) ,'stacked')
% legend(budgets.(budget_for(l))(4).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
% legend('toggle')
% xlabel('time [s]');ylabel(strcat(budget_for(l), " [mr]"));
% end
% hold off

for l=1:length(budget_for)
figure;
subplot(2,1,1);
title(strcat("Production of " , budget_for(l)))
hold on; grid on;
bar(VOCmr(2:end), prod.(budget_for(l)) ,'stacked')
legend(budgets.(budget_for(l))(4).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel(strcat((VOC)," [mr]"));ylabel(strcat(budget_for(l), " "));

subplot(2,1,2);
hold on; grid on;
title(strcat("Loss of " , budget_for(l)))
bar(VOCmr(2:end), loss.(budget_for(l)) ,'stacked')
legend(budgets.(budget_for(l))(4).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel(strcat((VOC)," [mr]"));ylabel(strcat(budget_for(l), " "));
end
hold off
%% Plot Zero-Value OH without VOC

figure;
subplot(2,1,1);
title(strcat("Production of OH without VOC"));
hold on; grid on;
bar([1,2],prod.OHzeroVOC,'stacked')
legend(budgets.OH(1).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel(strcat((VOC)," mr = 0"));ylabel(" OH ");

subplot(2,1,2);
hold on; grid on;
title(strcat("Loss of OH without VOC" ))
bar([1,2],loss.OHzeroVOC,'stacked')
legend(budgets.OH(1).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel(strcat((VOC)," mr = 0"));ylabel(" OH ");
hold off
