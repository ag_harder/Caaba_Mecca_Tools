if under_after(j)=="intern"
    
    figure;
    subplot(2,1,1);
    title(strcat("Production of " , budget_for(i) ," ",(under_after(j))))
    hold on; grid on;
    bar(log10(NOmr(2:end)), Prod_plot,'stacked')
    legend(Legend_prod,'Location','northeast','FontSize',9)%(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat(("log_1_0 NO [mr]"))); ylabel(strcat(budget_for(i), " [mr/s]"));

    subplot(2,1,2);
    hold on; grid on;
    title(strcat("Loss of " , budget_for(i) ," ",(under_after(j))))
    bar(log10(NOmr(2:end)), Loss_plot ,'stacked')
    legend(Legend_Loss,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat(" log_1_0 NO [mr]"));ylabel(strcat(budget_for(i), " [+/-mr]"));
    
    if save_figures == "Yes"
        filename = strcat(budget_for(i),"_budget_",(under_after(j)));
        saveas(gca, fullfile(fname, VOC,"Budget", filename), 'fig');
    end
    
else
     
    figure;
    subplot(2,1,1);
    title(strcat("Production of " , budget_for(i) ," ",(under_after(j))," lamp"))
    hold on; grid on;
    bar(VOCmr(2:end), Prod_plot,'stacked')
    legend(Legend_prod,'Location','northeast','FontSize',9)%(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat((VOC(v))," [mr]"));ylabel(strcat(budget_for(i), " [+/-mr]"));

    subplot(2,1,2);
    hold on; grid on;
    title(strcat("Loss of " , budget_for(i) ," ",(under_after(j))," lamp"))
    bar(VOCmr(2:end), Loss_plot ,'stacked')
    legend(Legend_Loss,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat((VOC(v))," [mr]"));ylabel(strcat(budget_for(i), " [+/-mr]"));
    
        if save_figures == "Yes"
        filename = strcat(budget_for(i),"_budget_",(under_after(j)));
        saveas(gca, fullfile(Budget_folder, filename), 'fig');
        end
    end