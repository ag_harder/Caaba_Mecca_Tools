clearvars
% 
stargate_path = 'C:\Users\F.Kunkler\Desktop\2020_RO2_HO2\stargate';
% stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';
filename = 'H2O_50lmin_30-Apr-2021_under_lamp_without_CH4';
filename = 'CH4_50lmin_15-Oct-2020_after_lamp';

load(strcat(stargate_path,'\output_matlab\output_',filename,'.mat'));
load(strcat(stargate_path,'\output_matlab\output_runs_',filename,'.mat'));

%% Read Output + create runs/output_struct
stargate_path = 'C:\Users\F.Kunkler\Desktop\2020_RO2_HO2\stargate';
% stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';
filename = 'zero_15-May-2021_intern_NOvar';

readin_model_output_runs(stargate_path,filename)
readin_model_output(stargate_path,filename)

load(fullfile(stargate_path,'output_matlab',strcat('output_',filename,'.mat')));
load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename,'.mat')));

%%
no_H2O = load(fullfile(stargate_path,'output_matlab',strcat('output_CH4_50lmin_15-Oct-2020_intern_NOvar_0H2O.mat')));
no_H2OH2O_CH4 = load(fullfile(stargate_path,'output_matlab',strcat('output_CH4_50lmin_15-Oct-2020_intern_NOvar_0H2OH2O.mat')));
NO_std = load(fullfile(stargate_path,'output_matlab',strcat('output_CH4_50lmin_15-Oct-2020_intern_NOvar.mat')));
no_VOC = load(fullfile(stargate_path,'output_matlab',strcat('output_CH4_50lmin_15-Oct-2020_intern_NOvar_noVOC.mat')));
no_HO2 = load(fullfile(stargate_path,'output_matlab',strcat('output_CH4_50lmin_15-Oct-2020_intern_NOvar_noHO2.mat')));
%% Create runs + output_struct
clearvars
stargate_path = 'C:\Users\F.Kunkler\Desktop\2020_RO2_HO2\stargate';
% stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';
VOC = "APINENE";
timestep_under = 1.1;timestep_after = 0.3;timestep_intern = 1.0;
filename_under = strcat(VOC,'_50lmin_08-Dec-2020_under_lamp');
filename_after = strcat(VOC,'_50lmin_08-Dec-2020_after_lamp');
filename_intern = strcat(VOC,'_50lmin_08-Dec-2020_intern_NOvar');

% save_addition = "with_CO";

% Loads structures for before and after lamp and combines them
output_struct = struct('under',[],'after',[]);
u = load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_under,'.mat')));
output_struct(1).under =u.output_struct;
u = load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_after,'.mat')));
output_struct.after =u.output_struct;
u = load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_intern,'.mat')));
output_struct.intern =u.output_struct;

runs = struct('under',[],'after',[]);
u = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_under,'.mat')));
runs(1).under =u.runs;
u = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_after,'.mat')));
runs.after =u.runs;
u = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_intern,'.mat')));
runs.intern =u.runs;
clear u  


% merge into one VM_all
VM_all.output_struct=output_struct;
VM_all.runs = runs;
[VM_all.sum_RR_runs,VM_all.sum_RR_output_struct]=sum_RR(stargate_path,timestep_under,timestep_after,timestep_intern,filename_under,filename_after,filename_intern);

if exist ('save_addition','var') == 1
    output_path=fullfile(stargate_path,'output_matlab',strcat('output_all_',(VOC),'_',save_addition,'.mat'));
else 
    output_path=fullfile(stargate_path,'output_matlab',strcat('output_all_',(VOC),'.mat'));
end



save(output_path,'VM_all')



CH4 = load(fullfile(stargate_path,'output_matlab',strcat('output_all_','CH4','.mat')));
C2H4 = load(fullfile(stargate_path,'output_matlab',strcat('output_all_','C2H4','.mat')));
BENZENE = load(fullfile(stargate_path,'output_matlab',strcat('output_all_','BENZENE','.mat')));
C5H8 = load(fullfile(stargate_path,'output_matlab',strcat('output_all_','C5H8','.mat')));
C3H6 = load(fullfile(stargate_path,'output_matlab',strcat('output_all_','C3H6','.mat')));
CO = load(fullfile(stargate_path,'output_matlab',strcat('output_all_','CO','.mat')));
C3H8 = load(fullfile(stargate_path,'output_matlab',strcat('output_all_','C3H8','.mat')));
APINENE = load(fullfile(stargate_path,'output_matlab',strcat('output_all_','APINENE','.mat')));
%% Timeresolved Budget of a single run
budget_path = 'C:\Users\F.Kunkler\Desktop\2020_RO2_HO2\Budget';
% budget_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\Budget';
runnum = 6;
filename = 'CH4_50lmin_15-Oct-2020_after_lamp';
budgets_run = plot_budget_run (budget_path,filename,runnum,"OH","CH3O2");

%% Mixingratioresolved (VOC) Budget plot 
budget_path = 'C:\Users\F.Kunkler\Desktop\2020_RO2_HO2\Budget';
% budget_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\Budget';

budgets = plot_budget (stargate_path,budget_path,filename,"CH4","OH","CH3O2");



%% Read Budgets
filename='zero_30-Apr-2021_intern_NOvar'; budget_path = 'C:\Users\F.Kunkler\Desktop\2020_RO2_HO2\Budget';
budget_for = ["OH"];
load(strcat(stargate_path,'\output_matlab\output_',filename,'.mat'));

for k=1:length(budget_for) 
    datfile_path(k)=strcat(budget_path,'\budgetUnKnwn',budget_for(k),'.dat');
end

for l=1:length(budget_for) 
for k=1:length(output_struct)
    budgets.(budget_for(l))(k)= budgetcalc(datfile_path(l),output_struct(k)); 
end
end

%% Sum RR runs
stargate_path = 'C:\Users\F.Kunkler\Desktop\2020_RO2_HO2\stargate';
% stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';
filename_under = 'CH4_50lmin_06-Oct-2020_under_lamp';
filename_after = 'CH4_50lmin_06-Oct-2020_after_lamp';

sum_RR_runs=sum_RR_runs(stargate_path,filename_under,filename_after);
%% Stuff
% doc switch






%TO-DO
% plot_budged_manual muss die Legende angepasst werden.. 
% Vergleich voon INSGESAMT gebildeten OH und wieviel wir am Ende �brig haben.
