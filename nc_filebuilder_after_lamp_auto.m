clearvars;
stargate_path='C:\Users\F.Kunkler\Desktop\2020_RO2_HO2\stargate\';
stargate_path='C:\Users\f.kunkler\Documents\Seafile\2020_RO2_HO2\stargate\';

VOC = "H2O";

filename = convertStringsToChars(strcat(VOC,'_50lmin_30-Apr-2021_under_lamp_without_CH4'));
filename = convertStringsToChars(strcat('2021_Mixed_VOC_30-Aug-2021_under_lamp_new_J'));
savename = convertStringsToChars(strcat(VOC,'_50lmin_30-Apr-2021_after_lamp_without_CH4'));
savename = convertStringsToChars(strcat('2021_Mixed_VOC_30-Aug-2021_after_lamp_new_J'));

%loads Output_structs of the run UNDER the Lamp
load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename,'.mat')));
load(fullfile(stargate_path,'output_matlab',strcat('output_',filename,'.mat')));


%constants
p0=101325 ; t0=298.15; kB=1.380649E-23; % Pa, K, J/K
lozahl=(p0/(t0*kB)/1000000); %1/cm^3
H2Opercent = 0.01;
H2Oin = (lozahl*H2Opercent)/lozahl;
%% input test values
temp=298.15; %K
press=101325; %Pa
CO2 = 0;
j_H2O = 0; %



%% Goes into the Output_struct to read which fields are in and delete the Reactionrates


fieldnames_output = fieldnames(output_struct);
fieldnames_RR = fieldnames_output(contains(fieldnames(output_struct),'RR'));
fieldnames_remove = cat(1,fieldnames_RR,'Tgps','nml_press','nml_temp'); %'MaxTime'
output_struct_slim = removevars(struct2table(output_struct),fieldnames_remove);
output_struct_slim = table2struct(output_struct_slim);

%Input off the fields into struct
length_array = length(output_struct_slim);
fieldnames_input = fieldnames(output_struct_slim);
model_input = struct;
for k=1: length(fieldnames_input)
   model_input.(string(fieldnames_input(k))) = [output_struct(1:(length_array)).(string(fieldnames_input(k)))]; 
end

%% Standard
temp=298.15; %K
press=101325; %Pa
j_H2O = 0; %



%final input !!!if CH4 is a field in output_struct it ignores the CH4 = 0!!
if isfield ( output_struct,'CH4')
    model_input.nml_press=repmat(press,[1,length_array]); %Pa standard
    model_input.nml_temp=repmat(temp,[1,length_array]); %K  standard
%     model_input.H2O=repmat(H2Oin,[1,length_array]); % 1% H2O 
    model_input.j_H2O=repmat(j_H2O,[1,length_array]); % J x sigma
    model_input.Tgps=repmat(1:length_array,[1,1]);
else
    model_input.nml_press=repmat(press,[1,length_array]); %Pa standard
    model_input.nml_temp=repmat(temp,[1,length_array]); %K  standard
%     model_input.H2O=repmat(H2Oin,[1,length_array]); % 1% H2O 
    model_input.j_H2O=repmat(j_H2O,[1,length_array]); % J x sigma
    model_input.Tgps=repmat(1:length_array,[1,1]);
    model_input.CH4 = repmat(0,[1,length_array]);
end

% model_input.CH4 = repmat(CH4,[1,length_array]);
% model_input.CO = repmat(CO,[1,length_array]);

%% Create netCDF file from MatLab dataset

fname    = strcat(savename,'.nc');


varnames=fields(model_input);

if exist([stargate_path,fname]) %#ok<EXIST>
    fclose('all');
    clear mex %#ok<CLMEX>
    delete([stargate_path,fname]);
end

ncid     = netcdf.create([stargate_path,fname],'NC_NOCLOBBER'); % Open netCDF file.
len_data = [size(model_input.Tgps,2),size(varnames,1)];   % Define the dimensions of the variable
%dim      = len_data(1)+1;
%dimid    = netcdf.defDim(ncid,'T_AX',len_data);
dimid    = netcdf.defDim(ncid,'time',netcdf.getConstant('NC_UNLIMITED'));

for i = 1 : len_data(2)                          % Define a new variable in the file.
    varID   = netcdf.defVar(ncid,varnames{i},'double',dimid);
    netcdf.endDef(ncid);                    % Leave define mode and enter data mode to write data.
    netcdf.putVar(ncid,varID,0,len_data(1), eval(['model_input.',varnames{i}]));      % Write data to variable.
    netcdf.reDef(ncid);                     % Leave data mode and re-enter define mode
    % netcdf.putAtt(ncid,varID,'units','mixing ratio'); % define attribute for missing value
end
netcdf.close(ncid);

