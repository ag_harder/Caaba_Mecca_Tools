function [] = create_netcdf(model_input,data_name,stargate_path)
%% delete negative data
temp_neg_data(length(model_input.Tgps))=false;
field_names=fieldnames(model_input);

for k=1:length(model_input.Tgps)
    for l=1:length(field_names)
        if model_input.(field_names{l})<0
            temp_neg_data(k)=true;
        end
    end
end

for l=1:length(field_names)
    model_input.(field_names{l})(temp_neg_data)=[];
end

save([stargate_path,'input_mat\input_data_',data_name,'.mat'],'model_input');

%% Create netCDF file from MatLab dataset
varnames=fields(model_input);
fname    = ['input_',data_name,'.nc'];

if exist([stargate_path,fname],'file')
    fclose('all');
    clear mex
    delete([stargate_path,fname]);
end

ncid     = netcdf.create([stargate_path,fname],'NC_NOCLOBBER'); % Open netCDF file.
len_data = [size(model_input.Tgps,2),size(varnames,1)];   % Define the dimensions of the variable
%dim      = len_data(1)+1;
%dimid    = netcdf.defDim(ncid,'T_AX',len_data);
dimid    = netcdf.defDim(ncid,'time',netcdf.getConstant('NC_UNLIMITED'));

for i = 1 : len_data(2)                          % Define a new variable in the file.
    varID   = netcdf.defVar(ncid,varnames{i},'double',dimid);
    netcdf.endDef(ncid);                    % Leave define mode and enter data mode to write data.
    netcdf.putVar(ncid,varID,0,len_data(1), eval(['model_input.',varnames{i}]));      % Write data to variable.
    netcdf.reDef(ncid);                     % Leave data mode and re-enter define mode
    % netcdf.putAtt(ncid,varID,'units','mixing ratio'); % define attribute for missing value
end
netcdf.close(ncid);

end

