clearvars;
My_Config;

%constants
p0=101325 ; t0=298.15; kB=1.380649E-23; % Pa, K, J/K
lozahl=(p0/(t0*kB)/1000000); %1/cm^3
at=[];lt=[];OH_prod=[];



press = 101325; %Pa
temp=298.15; %K
particle_dens = lozahl*(press/p0)*(t0/temp);

lamp_flow = 6E-6; % 1/s UVH lamp (Henning Finkenzeller 2021)
wq_H2O = 7.2E-20; % at XX nm?
j_H2O = 0; % 
j_O1D = 6E-6; % 1/s jO3(Henning Finkenzeller 2021)
j_ = 7E-5; % 1/s UVX at 400Hz x 4mJ (Henning Finkenzeller 2021)

% Reaktion von OH und CO muss innerhalb einer Stunde mehr als 30 ppb CO2
% liefern...
cair = particle_dens;
k_CO_OH = 1.57E-13+cair*3.54E-33; % cair = air concentration [molecules/cm3]
t_CO_OH_h = 1; %h time in chamber in hours
t_CO_OH_s = t_CO_OH_h *60 * 60; %s in seconds


%k_CO_OH*pd_OH*t_CO_OH_s

%%Choose VOC
var_name_VOC=["C3H8"];
H2Opercent = 0.01;
H2Oin = 5E-3;
CH4 = 0;
CO = 100E-9;
CO2 = 0;
O3 = 100E-9;
%OH_i = 1E-7/particle_dens;

percant = 0.01; %how many OH should be left, needed for the calculation of the lifetime 
prod_OH = lamp_flow*wq_H2O*H2Opercent*lozahl; %[1/s]


%for schleife
for k=1:length(var_name_VOC)
  if strcmp(var_name_VOC(k),'CH4') %Methan 
    kVOC= 1.85E-20*exp(2.82*log(temp)-987./temp);
  elseif strcmp(var_name_VOC(k),'C2H6') %Ethan
    kVOC= 1.49E-17*temp*temp*exp(-499./temp);
  elseif strcmp(var_name_VOC(k),'C3H8') %Propan
    kVOC1= 4.50E-18*temp*temp*exp(253./temp); %Isoprop
    kVOC2= 2*4.49E-18*temp*temp*exp(-320./temp); %n-prop
    kVOC = kVOC1+kVOC2;
  elseif strcmp(var_name_VOC(k),'C2H4'); %Ethen
    kVOC= third_order_reaction_rate(temp,lozahl,8.6E-29,3.1,9.E-12,0.85,0.48);
  elseif strcmp(var_name_VOC(k),'C3H6') %Propen
    kVOC= third_order_reaction_rate(temp,lozahl,8.6E-27,3.5,3.E-11,1.,0.5);
  elseif strcmp(var_name_VOC(k),'BENZENE') %Benzol
    kVOC= 2.3E-12*exp(-190/temp);
  elseif strcmp(var_name_VOC(k),'TOLUENE') %Toluol
    kVOC= 1.8E-12*exp(340/temp);
  elseif strcmp(var_name_VOC(k),'C5H8') %Isopren
    kVOC=2.7E-11*exp(390./temp);
  elseif strcmp(var_name_VOC(k),'APINENE') %a-Pinen
    kVOC=1.2E-11*exp(440./temp);    
  elseif strcmp(var_name_VOC(k),'LIMONENE') %Limonen
    kVOC=1;
  elseif strcmp(var_name_VOC(k),'BPINENE') %beta-Pinen
      kVOC=1.47E-11*EXP(467./TEMP)*(0.8326*0.3+0.068)/(0.8326+0.068);
  else 
      kVOC=1
  end
  
end


lamp_length = 3.5; channel_width = 1.6; channel_height = 1.6; after_length = 1.18;    %all cm
cs_channel = channel_height*channel_width; %cm^2
volume_flow=1;
flow_speed= ((volume_flow/60)/cs_channel) *(temp/273.15)*(101325/press); %cm/s
lamp_time = lamp_length/flow_speed; %s
after_time= after_length/flow_speed; %s 

concVOC=(log(percant/1)/(-kVOC*after_time));
VOCa = concVOC/lozahl;
%f�r BENZENE testweise Sehr Hohe MR um BZBIPERO2 zu pushen!
% VOCmr = [0,VOCa,2*VOCa,4*VOCa,6*VOCa,8*VOCa,12*VOCa,16*VOCa,20*VOCa,30*VOCa,40*VOCa];
VOCmr = [0,0.1*VOCa,0.2*VOCa,0.5*VOCa,0.8*VOCa,0.9*VOCa,VOCa,1.1*VOCa,1.2*VOCa,1.5*VOCa,8*VOCa,10*VOCa];

at=[at,after_time];
lt=[lt,lamp_time];
OH=prod_OH*lamp_time;
OH_prod =[OH_prod,OH];




%% input test values
% COmr = [0,0.001,0.002,0.004,0.006,0.008,0.01,0.012,0.016,0.02,0.03];


length_array=length(VOCmr);
length_array=1;

model_input.nml_press=repmat(press,[1,length_array]); %Pa standard
model_input.nml_temp=repmat(temp,[1,length_array]); %K  standard
model_input.H2O=repmat(H2Oin,[1,length_array]); % 1% H2O 
model_input.j_H2O=repmat(j_H2O,[1,length_array]); % J x sigma
model_input.j_O1D=repmat(j_O1D,[1,length_array]);
model_input.Tgps=repmat(1:length_array,[1,1]);
model_input.CO2=repmat(CO2,[1,length_array]);
model_input.CO=repmat(CO,[1,length_array]);
%model_input.(var_name_VOC)=VOCmr;
model_input.CH4=repmat(CH4,[1,length_array]);
model_input.O3= repmat(O3,[1,length_array]);

% model_input.(var_name_VOC)=COmr
% for k=1:length(var_name_VOC)
%     VOCin=zeros(1,length(VOCmr2));
%     VOCin(k*length(VOCmr)-(length(VOCmr)-1):k*length(VOCmr))=VOCmr2(k*length(VOCmr)-(length(VOCmr)-1):k*length(VOCmr));
%     model_input.(var_name_VOC(k))=VOCin;
% end


% runtime_str= "'0.001 seconds'";
% timesteplen_str= "'0.0001 seconds'";
% model_input.runtime_str=str2double(repmat(runtime_str,[1,length_array]));
% model_input.timesteplen_str=str2double(repmat(timesteplen_str,[1,length_array]));

%% Create netCDF file from MatLab dataset

fname    = convertStringsToChars(strcat('CO_CO2_Run_CLOUD','.nc'));


varnames=fields(model_input);

if exist([stargate_path,fname]) %#ok<EXIST>
    fclose('all');
    clear mex %#ok<CLMEX>
    delete([stargate_path,fname]);
end

ncid     = netcdf.create([stargate_path,fname],'NC_NOCLOBBER'); % Open netCDF file.
len_data = [size(model_input.Tgps,2),size(varnames,1)];   % Define the dimensions of the variable
%dim      = len_data(1)+1;
%dimid    = netcdf.defDim(ncid,'T_AX',len_data);
dimid    = netcdf.defDim(ncid,'time',netcdf.getConstant('NC_UNLIMITED'));

for i = 1 : len_data(2)                          % Define a new variable in the file.
    varID   = netcdf.defVar(ncid,varnames{i},'double',dimid);
    netcdf.endDef(ncid);                    % Leave define mode and enter data mode to write data.
    netcdf.putVar(ncid,varID,0,len_data(1), eval(['model_input.',varnames{i}]));      % Write data to variable.
    netcdf.reDef(ncid);                     % Leave data mode and re-enter define mode
    % netcdf.putAtt(ncid,varID,'units','mixing ratio'); % define attribute for missing value
end
netcdf.close(ncid);

