clearvars 
stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';
VOC =["CH4"];
load(fullfile(stargate_path,'output_matlab',strcat('output_all_',(VOC),'.mat')));

under_after=["under","after"];

for m=1:length(under_after)
figure
x = 'CH4'; y = 'OH';
x_u = '[mr]'; y_u='[mr]';
hold on; grid on
plot([VM_all.output_struct.(under_after(m))(2:end).CH4],[VM_all.output_struct.(under_after(m))(2:end).OH],'Color',[0.4940 0.1840 0.5560],'linestyle','none','marker','.','MarkerSize',10)
title([strcat(x, " vs ",y," ",under_after(m)," lamp")])
xlabel([x,' ',x_u]); ylabel([y,' ',y_u])
legend ()
end