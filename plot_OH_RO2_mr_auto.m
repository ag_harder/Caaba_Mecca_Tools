clearvars
stargate_path = 'C:\Users\F.Kunkler\Desktop\VM\stargate';

%Plots the timeresolved change of OH and RO2 mixingratio troughout the run

VOC =["C3H8"];
under_after = ["under","after","intern"];
load(fullfile(stargate_path,'output_matlab',strcat('output_all_',VOC,'.mat')));
save_figures = ["Yes"];


    if save_figures == "Yes" 
    fname = 'C:\Users\F.Kunkler\Desktop\VM\Figures';
    VOC_folder = fullfile(fname,VOC);
     
        if ~exist(VOC_folder, 'dir')  %creates the save folders
            mkdir(VOC_folder)
        end
    end
    
%% 

switch VOC
    case "Dummy"
        RO2 = [];
        side_products = [];  
        intern_RO2_product = [];   % 
        intern_RO2_side =[]; % Preproducts..  
    case "CH4"
        RO2 = ["CH3O2"];
        side_products = ["CH3OOH","HCHO"];
    case "C2H4"
        RO2 = ["HOCH2CH2O2"];
        side_products = ["HOCH2CH2O","HOCH2CHO","ETHGLY","HYETHO2H"];
    case "BENZENE"
        RO2 = ["BZBIPERO2"];
        side_products = ["PHENOL","BZEPOXMUC","CATECHOL","BZEMUCO2","BZEMUCCO3"];
    case "C5H8"
        RO2 = ["LISOPACO2","ISOPBO2","LDISOPACO2","ISOPDO2","LISOPEFO2"];
        side_products = [""];
    case "C3H6"
        RO2 = ["HYPROPO2"];
        side_products = ["HYPROPO2H","PROPOLNO3"];  
    case "C3H8"
        RO2 = ["IC3H7O2","NC3H7O2"];
        side_products = ["NC3H7OOH","IC3H7OOH"];  
        intern_RO2_product = ["C2H5CHO","CH3COCH3","HCHO"];   % 
        intern_RO2_side =["NC3H7NO3","IC3H7NO3","CH3COCH2O2","CH3CO","IPROPOL","HYPERACET","NOA","C2H5CO3"]; % Preproducts..  
    case "APINENE"
        RO2 = ["LAPINABO2","ROO6R1O2","OHMENTHEN6ONEO2","PINALO2"];
        side_products = ["MENTHEN6ONE","LAPINABOOH","PINAL","PINALOOH"];  
        intern_RO2_product = ["PINAL","C106O2","OH2MENTHEN6ONE"];   % Pinal product of LAPINABO2+NO, C106O2 product of PINAL+NO
        intern_RO2_side =["LAPINABNO3","PINALNO3"]; % Preproducts..  

end

y_variables = ["OH", RO2];
x_variables = ["runtime"];

for m=1:length(under_after)
for l=1:length(y_variables)
    if length(x_variables)==1
        x=x_variables(1);
    elseif length(x_variables)==2
        x=x_variables(l); %L
    end
    
    figure
    grid on; hold on;

%     if y_variables(l)== "CH3O2"
%         a=2;
%     else
%         a=1;
%     end
    
    
    for k=2:length(fields(VM_all.runs.(under_after(m))))
       if k<10
         run = strcat('run000',num2str(k));
       elseif k>=10
         run = strcat('run00',num2str(k));
       elseif k>=100
         run = strcat('run0',num2str(k));
       elseif k>=1000
         run = strcat('run',num2str(k));
       end
       
    y = y_variables(l);
    if under_after(m) == "intern"
        title((strcat((y_variables(l))," progress ", under_after(m)," (at 50 l/min VOC=",VOC,")")))
    else
        title((strcat((y_variables(l))," progress ", under_after(m)," lamp"," (at 50 l/min VOC=",VOC,")")))
    end
    x_u = '[s]';
    y_u = '[mr]';
    xlabel(strcat(x," ",x_u)); ylabel(strcat(y," ",y_u));

 %('Name','OH vs runtime','Color','w')

plot ([VM_all.runs.(under_after(m)).(run).(x)],[VM_all.runs.(under_after(m)).(run).(y_variables(l))],'marker','.','MarkerSize',15)
if under_after(m) =="intern"
   for_legend(k-1)= strcat("NO mr = ",num2str([VM_all.output_struct.(under_after(m))(k).NO]));
else
   for_legend(k-1)= strcat(VOC," mr = ",num2str([VM_all.output_struct.(under_after(m))(k).(VOC)])); 
end

    end
legend ( for_legend,'Location','Best','FontSize',7)    

    if save_figures == "Yes"
        filename = strcat(y_variables(l),"_progress_",(under_after(m)));
        saveas(gca, fullfile(fname, VOC, filename), 'fig');
    end
    
end
end



