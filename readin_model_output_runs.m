function [output_mat,output_struct] = readin_model_output_runs(stargate_path,filename,filter_variable,output_path)
% function to read timestep output in and save caaba multirun output from given path
% Script from Felix, inspired from Sebastians, which was modified by Roland 

% RRG Values sind in der Einheit 1/s! Unabh�ngig von der Einheit die man f�r die anderen Substanzen verwendet. 

if nargin < 4
    output_path=fullfile(stargate_path,'output_matlab',strcat("output_runs_",filename,".mat"));
end

if nargin < 3
    filter_variable=true;
end

%%
%%finds runs
runs_path=fullfile(stargate_path,filename,'\runs\');
d=dir2(runs_path);
%% Load nc files

runs=struct;
for l=1:length(d)
    temp_var_name=['run',(d(l).name)];
    runs.(temp_var_name)=[];
end

for k=1:length(d)  %runs as often as we have runs numbers
    [valname, valdata]=read_nc(fullfile(runs_path,d(k).name,'\caaba_mecca.nc'));
    if size(valname,1) == size(valdata,1)
    caaba_mecca = array2table(valdata');
    else     % Right orientation    
    caaba_mecca = array2table(valdata);
    end
    caaba_mecca_name = valname;
    

    [valname, valdata]=read_nc(fullfile(runs_path,d(k).name,'\caaba_mecca_rr.nc'));
    if size(valname,1) == size(valdata,1)
    caaba_mecca_rr = array2table(valdata');
    else     % Right orientation    
    caaba_mecca_rr = array2table(valdata);
    end
    caaba_mecca_rr_name = valname;
    
    [valname, valdata]=read_nc(fullfile(runs_path,d(k).name,'\caaba_messy.nc'));
    if size(valname,1) == size(valdata,1)
    caaba_messy = array2table(valdata');
    else     % Right orientation    
    caaba_messy = array2table(valdata);
    end
    caaba_messy_name = valname;
    
    
    caaba_mecca.Properties.VariableNames = caaba_mecca_name; %naming columns
    caaba_mecca_rr.Properties.VariableNames = caaba_mecca_rr_name;
    caaba_messy.Properties.VariableNames = caaba_messy_name;
    data_all = cat(2,caaba_mecca_rr,caaba_mecca); %combines the data from 2 files
    
 
    %% Filter Zero / unchanged Values
if filter_variable==true
    index_diff=[];
    for b=1:length(fieldnames(data_all))-3 %runs as often as variable names
        % RR: bei if Anstelle von : sum(diff(table2array(output_struct(:,b)))==0  jetzt length( unique( table2array(output_struct(:,b)) ) )==1        
        if sum((table2array(data_all(:,b))))==0 % FK bei unique werden die festen Konzentrationen der VOC rausgel�scht.. 
            data_all{:,b}=NaN;
            index_diff=[index_diff,b]; %#ok<AGROW>
        end
    end
    data_all(:,index_diff)=[];
end

%%
%Combine and rearrangement/ reading out time
data_all = cat(2,caaba_messy(:,1:2),data_all); % final combine because the filter would delete the messy data

%reads the time out of caaba_messy.nc and works with it..
timestamp = ncread((fullfile(runs_path,d(k).name,'\caaba_messy.nc')),'time');
starttime = timestamp(1);
timestamp_corr = timestamp-starttime;
data_all.runtime = timestamp_corr;

data_all = [data_all(:,end) data_all(:,1:end-1)]; %rearrange columns
data_all=table2struct(data_all); %turns table to structur 
%%
% to write in the right field of runs (didnt figured a better way out..)
if k<10
    run = strcat('run000',num2str(k));
    runs.(run) = data_all;
elseif k>=10
    run = strcat('run00',num2str(k));
    runs.(run) = data_all;
elseif k>=100
    run = strcat('run0',num2str(k));
    runs.(run) = data_all;
elseif k>=1000
    run = strcat('run',num2str(k));
    runs.(run) = data_all;
end
 

end
output_mat=output_path;
save(output_path,'runs');
output_struct=runs;
end


