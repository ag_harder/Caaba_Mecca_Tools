
function [budgets] = plot_budget(stargate_path,budget_path,filename,VOC,budgetfor1,budgetfor2)

%plot_budget_runs(budget_path,filename,runnum,budgetfor1,budgetfor2,budgetfor3)
%
%
%function to plot budgets of a single run !TIMERESOLVED! for up to 3 species vs the runtime
%       runnum = Run-number you want to plot e.g. 1
%       budgetforX are the species e.g. OH or CH4..
%
%                       ATTENTION !!!! 
%you may need to config the datapath in the function

load(strcat(stargate_path,'\output_matlab\output_',filename,'.mat'));

budgets = struct;
%% Choose Run and what u want 

if nargin == 6
    budget_for = [budgetfor1,budgetfor2];
elseif nargin == 5
    budget_for = [budgetfor1];
end

for k=1:length(budget_for) 
    datfile_path(k)=strcat(budget_path,'\budgetUnKnwn',budget_for(k),'.dat');
end
%% get Values for Budgets

for l=1:length(budget_for) 
for k=1:length(output_struct)
    budgets.(budget_for(l))(k)= budgetcalc(datfile_path(l),output_struct(k)); 
end
end


%% 
%in ProdRxn Reaktionsgleichungen der Produktion
VOCmr = [output_struct.(VOC)].'; %[] holt die VOC mr aus dem zugehörigen raus

%% in Loss all loss data per timestep same for prod
prod = struct; loss = struct;
for l=1:length(budget_for) 
    if budget_for(l)=="OH"
        if l==1 
            for k=1:length(VOCmr)
                if k == 1
                    prod0(:,(k)) = budgets.(budget_for(l))(k).Prod;
                    loss0(:,(k)) = budgets.(budget_for(l))(k).Loss;
                    else
                  
                    prod1(:,(k-1)) = budgets.(budget_for(l))(k).Prod;
                    loss1(:,(k-1)) = budgets.(budget_for(l))(k).Loss;      
                end
            end
                    prod0 = prod0.'; prod0(2,:)=repmat(0,[1,length(prod0)]);
                    prod1 = prod1.';
                    loss0 = loss0.'; loss0(2,:)=repmat(0,[1,length(loss0)]);
                    loss1 = loss1.';
                    prod.OHzeroVOC = prod0;prod.(budget_for(l)) = prod1;
                    loss.OHzeroVOC = loss0;loss.(budget_for(l)) = loss1;
                   
        elseif l==2 
            for k=1:length(VOCmr)
            prod2(:,(k)) = budgets.(budget_for(l))(k).Prod;
            loss2(:,(k)) = budgets.(budget_for(l))(k).Loss;   
            end
            prod2=prod2.'; prod.(budget_for(l)) = prod2;
            loss2 = loss2.';loss.(budget_for(l)) = loss2;
        
        elseif l==3 
         for k=1:length(VOCmr)
            prod3(:,(k-1)) = budgets.(budget_for(l))(k).Prod;
            loss3(:,(k-1)) = budgets.(budget_for(l))(k).Loss;   
         end
            prod3=prod3.'; prod.(budget_for(l)) = prod3;
            loss3 = loss3.';loss.(budget_for(l)) = loss3;
        end
    else  
         if l==1 
            for k=2:length(VOCmr)
            prod1(:,(k-1)) = budgets.(budget_for(l))(k).Prod;
            loss1(:,(k-1)) = budgets.(budget_for(l))(k).Loss;   
            end
            prod1 = prod1.';prod.(budget_for(l)) = prod1;
            loss1 = loss1.';loss.(budget_for(l)) = loss1;
        elseif l==2 
            for k=2:length(VOCmr)
            prod2(:,(k-1)) = budgets.(budget_for(l))(k).Prod;
            loss2(:,(k-1)) = budgets.(budget_for(l))(k).Loss;   
            end
            prod2=prod2.'; prod.(budget_for(l)) = prod2;
            loss2 = loss2.';loss.(budget_for(l)) = loss2;
        
        elseif l==3 
         for k=2:length(VOCmr)
            prod3(:,(k-1)) = budgets.(budget_for(l))(k).Prod;
            loss3(:,(k-1)) = budgets.(budget_for(l))(k).Loss;   
         end
            prod3=prod3.'; prod.(budget_for(l)) = prod3;
            loss3 = loss3.';loss.(budget_for(l)) = loss3;
         end
    end
end

%% Plot of Loss and Prod


%if u want all 4 in 1

% figure;
% for l=1:length(budget_for)
% if l==1
%     subplot(4,1,l);
% elseif l==2
%     subplot(4,1,3)
% end
% title(strcat("Production of " , budget_for(l)))
% hold on; grid on;
% bar(VOCmr(2:end), prod.(budget_for(l)) ,'stacked')
% legend(budgets.(budget_for(l))(4).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
% legend('toggle')
% xlabel('time [s]');ylabel(strcat(budget_for(l), " [mr]"));
% subplot(4,1,2*l);
% hold on; grid on;
% title(strcat("Loss of " , budget_for(l)))
% bar(VOCmr(2:end), loss.(budget_for(l)) ,'stacked')
% legend(budgets.(budget_for(l))(4).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
% legend('toggle')
% xlabel('time [s]');ylabel(strcat(budget_for(l), " [mr]"));
% end
% hold off

for l=1:length(budget_for)
figure;
subplot(2,1,1);
title(strcat("Production of " , budget_for(l)))
hold on; grid on;
bar(VOCmr(2:end), prod.(budget_for(l)) ,'stacked')
legend(budgets.(budget_for(l))(4).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel(strcat((VOC)," [mr]"));ylabel(strcat(budget_for(l), " "));

subplot(2,1,2);
hold on; grid on;
title(strcat("Loss of " , budget_for(l)))
bar(VOCmr(2:end), loss.(budget_for(l)) ,'stacked')
legend(budgets.(budget_for(l))(4).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel(strcat((VOC)," [mr]"));ylabel(strcat(budget_for(l), " "));
end
hold off
%% Plot Zero-Value OH without VOC

figure;
subplot(2,1,1);
title(strcat("Production of OH without VOC"));
hold on; grid on;
bar([1,2],prod.OHzeroVOC,'stacked')
legend(budgets.OH(1).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel(strcat((VOC)," mr = 0"));ylabel(" OH ");

subplot(2,1,2);
hold on; grid on;
title(strcat("Loss of OH without VOC" ))
bar([1,2],loss.OHzeroVOC,'stacked')
legend(budgets.OH(1).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel(strcat((VOC)," mr = 0"));ylabel(" OH ");
hold off


