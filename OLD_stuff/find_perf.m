clearvars;
stargate_path='C:\Users\F.Kunkler\Desktop\VM\stargate\';

%constants
p0=101325 ; t0=298.15; kB=1.380649E-23; % Pa, K, J/K
lozahl=(p0/(t0*kB)/1000000); %1/cm^3
VOCmr=[];
rt=[];a = [];
%%Choose VOC
var_name_VOC=["CH4"];
H2Opercent = 0.01;
H2Oin = (lozahl*H2Opercent)/lozahl;



%calculate concentration of VOC to react with OH -1=log([OH]t/[OH]0)
%for a Halflifetime of 10 ms
temp=298.15; %K
press = 1013.15;

%for schleife
for k=1:length(var_name_VOC)
  if strcmp(var_name_VOC(k),'CH4') %Methan 
    kVOC= 1.85E-20*exp(2.82*log(temp)-987./temp);
  elseif strcmp(var_name_VOC(k),'C2H6') %Ethan
    kVOC= 1.49E-17*temp*temp*exp(-499./temp);
  elseif strcmp(var_name_VOC(k),'C3H8') %Propan
    kVOC= 4.50E-18*temp*temp*exp(253./temp); %Isoprop
    kVOC2= 2*4.49E-18*temp*temp*exp(-320./temp); %n-prop
  elseif strcmp(var_name_VOC(k),'C2H4'); %Ethen
    kVOC= third_order_reaction_rate(temp,lozahl,8.6E-29,3.1,9.E-12,0.85,0.48);
  elseif strcmp(var_name_VOC(k),'C3H6') %Propen
    kVOC= third_order_reaction_rate(temp,lozahl,8.6E-27,3.5,3.E-11,1.,0.5);
  elseif strcmp(var_name_VOC(k),'BENZENE') %Benzol
    kVOC= 2.3E-12*exp(-190/temp);
  elseif strcmp(var_name_VOC(k),'TOLUENE') %Toluol
    kVOC= 1.8E-12*exp(340/temp);
  elseif strcmp(var_name_VOC(k),'C5H8') %Isopren
    kVOC=2.7E-11*exp(390./temp);
  elseif strcmp(var_name_VOC(k),'APINENE') %a-Pinen
    kVOC=1.2E-11*exp(440./temp);    
  elseif strcmp(var_name_VOC(k),'limonene') %Limonen
    kVOC=1; 
  end
  
end

%% Constants

lamp_length = 3.5; channel_width = 1.6; channel_height = 1.6; after_length = 1.18;    %all cm
cs_channel = channel_height*channel_width; %cm^2