clearvars
load('C:\Users\Felix\Desktop\Mastersachen\VM\stargate\output_matlab\output_runs_RO2_Felix_Multi_after_lamp.mat');
load('C:\Users\Felix\Desktop\Mastersachen\VM\stargate\output_matlab\output_RO2_Felix_Multi_under_lamp.mat');
datfile_CH3O2='C:\Users\Felix\Desktop\Mastersachen\VM\Budget\budgetUnKnwnCH3O2.dat';
datfile_OH='C:\Users\Felix\Desktop\Mastersachen\VM\Budget\budgetUnKnwnOH.dat';

budget_CH3O2=struct;
budget_OH=struct;

%% Choose Run
runnum=4; % runnumber u want to lookin

runnum=strcat('run000',num2str(runnum));

%% get Values for Budgets
%for CH3O2_budget
for k=1:length(runs.(runnum))
    budget_CH3O2(k).(runnum)= budgetcalc(datfile_CH3O2,runs.(runnum)(k));
end

%for OH_budget
for k=1:length(runs.(runnum))
    budget_OH(k).(runnum)= budgetcalc(datfile_CH3O2,runs.(runnum)(k));
end

%% 
%in ProdRxn Reaktionsgleichungen der Produktion
% in ProdRxnNum die Nummer der Reaktion und in Prod der jeweilige Wert
time = [runs.(runnum).runtime].'; %[] holt das timerun-Field raus
a = budget_CH3O2(k).(runnum).Prod;  %k ist der zeitpunkt bzw Wert von time Reihe
b=time(k);

%% in Loss all loss data per timestep same for prod

prod = zeros (length(budget_CH3O2(3).(runnum).Prod),(length(runs.(runnum))-1));
loss = zeros (length(budget_CH3O2(3).(runnum).Loss),(length(runs.(runnum))-1));
for k=2:length(runs.(runnum))
    prod(:,(k-1)) = budget_CH3O2(k).(runnum).Prod;
    loss(:,(k-1)) = budget_CH3O2(k).(runnum).Loss;   
end
prod = prod.'; loss = loss.';
%% 
%fehlt noch Legende!

figure;
subplot(2,1,1);
title(['Budget for ']);
hold on; grid on;
bar(time(2:end), prod ,'stacked')
legend(budget_CH3O2(4).run0004.ProdRxn,'Location','northeast','FontSize',9)
subplot(2,1,2);
hold on; grid on;
bar(time(2:end), loss ,'stacked')
legend(budget_CH3O2(4).run0004.LossRxn,'Location','southeast','FontSize',9)


%% Plot the **** out of it
% figure
% hold on; grid on;
% bar(budget_CH3O2.