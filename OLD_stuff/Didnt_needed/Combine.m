%% Structure erstellen mit RR Werten als Summe �ber den ganzen Run und den Endmixingratios aus Outputstruct

clearvars

stargate_path = 'C:\Users\F.Kunkler\Desktop\VM\stargate';
% stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';
filename_under = 'CH4_50lmin_06-Oct-2020_under_lamp';
filename_after = 'CH4_50lmin_06-Oct-2020_after_lamp';


% Loads structures for before and after lamp and combines them
output_struct = struct('under',[],'after',[]);
u = load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_under,'.mat')));
output_struct(1).under =u.output_struct;
a = load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_after,'.mat')));
output_struct.after =a.output_struct;

runs = struct('under',[],'after',[]);
u = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_under,'.mat')));
runs(1).under =u.runs;
a = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_after,'.mat')));
runs.after =a.runs;

clear a u

