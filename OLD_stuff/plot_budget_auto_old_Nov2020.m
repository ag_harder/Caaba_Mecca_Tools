clearvars

stargate_path = 'C:\Users\F.Kunkler\Desktop\VM\stargate';
% stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';

VOC =["CH4"];
save_figures = ["No"];
under_after = ["intern"];

RO2 = ["OH","HO2","CH3O2","HOCH2CH2O2","LISOPACO2","ISOPBO2","LDISOPACO2","ISOPDO2","LISOPEFO2","BZBIPERO2","HYPROPO2"];

% Additional_Budgets =["PHENOL","BZEPOXMUC"];

for v=1:length(VOC)
    
    % Savepath and create the save folders
    if save_figures == "Yes" 
    fname = 'C:\Users\F.Kunkler\Desktop\VM\Figures';
    VOC_folder = fullfile(fname,VOC);
    Budget_folder = fullfile (VOC_folder,"Budget");
    
    
        if ~exist(VOC_folder, 'dir')  %creates the save folders
            mkdir(VOC_folder)
        end
        if ~exist(Budget_folder, 'dir')
            mkdir(Budget_folder)
        end
    end
    
    
budget_path = 'C:\Users\F.Kunkler\Desktop\VM\Budget\BudgetFiles';
load(fullfile(stargate_path,'output_matlab',strcat('output_all_',(VOC(v)),'.mat')));
budget_for=[];

for w=1:length(RO2)
    if isfield(VM_all.output_struct.(under_after(end)),(RO2(w)))==true
       budget_for= [budget_for,RO2(w)];
    end
end

% budget_for=[budget_for,Additional_Budgets]
%%
budgets = struct;

%% Choose Run and what u want 

for k=1:length(budget_for) 
    datfile_path(k)=strcat(budget_path,'\budgetUnKnwn',budget_for(k),'.dat');
end
%% get Values for Budgets
for m=1:length (under_after)
for l=1:length(budget_for) 
for k=1:length(VM_all.sum_RR_output_struct.(under_after(m)))
    budgets.(under_after(m)).(budget_for(l))(k)= budgetcalc(datfile_path(l),VM_all.sum_RR_output_struct.(under_after(m))(k)); 
end
end
end


%% 
%in ProdRxn Reaktionsgleichungen der Produktion

VOCmr = [VM_all.output_struct.(under_after(1)).(VOC(v))].'; %[] holt die VOC mr aus dem zugehörigen raus

if any(contains(under_after,["intern"]))== true
    NOmr = [VM_all.output_struct.intern.NO];
end

%% in Loss all loss data per timestep same for prod
prod = struct; loss = struct; legend_sum_loss = struct;legend_sum_prod=struct;

for l=1:length(budget_for)
    for m=1:length (under_after)
        
        % looks into budgets and picks the smallest number of Prod and Loss
        % reactions (we will loose reactions with E-30 or so)
        [x,vertical_fieldnum]=size(budgets.(under_after(m)).(budget_for(l)));
        A =[];C=[];
        for j=2:vertical_fieldnum
            [a,b]=size(budgets.(under_after(m)).(budget_for(l))(j).Prod);
            A=[A,a];
            [c,b]=size(budgets.(under_after(m)).(budget_for(l))(j).Loss);
            C=[C,c];
        end
        manual_end_prod =  min(A);
        manual_end_loss =  min(C);
        
        clear A a b C c x vertical_fieldnum
        
        
               if under_after(m)=="intern"
                length_loop = length(NOmr);
               else
                length_loop = length(VOCmr);
               end
            
            for k= 2:length_loop
                prod1=flip(budgets.(under_after(m)).(budget_for(l))(k).Prod);
                prod_cut(:,k-1) = flip(prod1(1:manual_end_prod));
                legendp1=flip(budgets.(under_after(m)).(budget_for(l))(k).ProdRxn);
                legendprod_cut(:,k-1) = flip(legendp1(1:manual_end_prod));
                loss1=flip(budgets.(under_after(m)).(budget_for(l))(k).Loss);
                loss_cut(:,k-1) = flip(loss1(1:manual_end_loss));  
                legendl1=flip(budgets.(under_after(m)).(budget_for(l))(k).LossRxn)
                legendloss_cut(:,k-1) = flip(legendl1(1:manual_end_loss));
            end
            
        prod_cut = prod_cut.';prod.(under_after(m)).(budget_for(l)) = prod_cut;
        loss_cut = loss_cut.';loss.(under_after(m)).(budget_for(l)) = loss_cut;
        legend_sum_loss.(under_after(m)).(budget_for(l))=legendloss_cut;
        legend_sum_prod.(under_after(m)).(budget_for(l))=legendprod_cut;
        prod1=[]; loss1=[]; loss_cut=[]; prod_cut =[];legendp1=[];legendl1=[],legendprod_cut={},legendloss_cut={};
    end
end

clear loss0 loss1 loss2 loss3 prod0 prod1 prod2 prod3 loss_cut
%% Plot of Loss and Prod


%if u want all 4 in 1

% figure;
% for l=1:length(budget_for)
% if l==1
%     subplot(4,1,l);
% elseif l==2
%     subplot(4,1,3)
% end
% title(strcat("Production of " , budget_for(l)))
% hold on; grid on;
% bar(VOCmr(2:end), prod.(budget_for(l)) ,'stacked')
% legend(budgets.(budget_for(l))(4).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
% legend('toggle')
% xlabel('time [s]');ylabel(strcat(budget_for(l), " [mr]"));
% subplot(4,1,2*l);
% hold on; grid on;
% title(strcat("Loss of " , budget_for(l)))
% bar(VOCmr(2:end), loss.(budget_for(l)) ,'stacked')
% legend(budgets.(budget_for(l))(4).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
% legend('toggle')
% xlabel('time [s]');ylabel(strcat(budget_for(l), " [mr]"));
% end
% hold off

for l=1:length(budget_for)
  for m=1:length (under_after)
      %to reduce the number of plottet and showed reactions to max 4
      legend_max = 4;
     [a,b]=size(prod.(under_after(m)).(budget_for(l)));
     [c,d]= size(loss.(under_after(m)).(budget_for(l)));
     %for prod
     if b>=5
         prod_plotend=(prod.(under_after(m)).(budget_for(l))(:,end-(legend_max-1):end));
         prod_plot1=sum(prod.(under_after(m)).(budget_for(l))(:,1:end-(legend_max)),2);
         prod_plot =[prod_plot1,prod_plotend];
         %legend
         legend_prod = budgets.(under_after(m)).(budget_for(l))(10).ProdRxn(end-(legend_max):end);
         legend_prod(1)={'Others'};
     else
         prod_plot=prod.(under_after(m)).(budget_for(l));
         legend_prod = budgets.(under_after(m)).(budget_for(l))(10).ProdRxn;
     end
     
     %for loss
     if d>=5
         loss_plotend=(loss.(under_after(m)).(budget_for(l))(:,end-(legend_max-1):end));
         loss_plot1=sum(loss.(under_after(m)).(budget_for(l))(:,1:end-(legend_max)),2);
         loss_plot =[loss_plot1,loss_plotend];
         %legend
         legend_loss = budgets.(under_after(m)).(budget_for(l))(10).LossRxn(end-(legend_max):end);
         legend_loss(1)={'Others'};
     else
         loss_plot = loss.(under_after(m)).(budget_for(l));
         legend_loss = budgets.(under_after(m)).(budget_for(l))(10).LossRxn;
     end
if under_after(m)=="intern"
    
    figure;
    subplot(2,1,1);
    title(strcat("Production of " , budget_for(l) ," ",(under_after(m))))
    hold on; grid on;
    bar(log10(NOmr(2:end)), prod_plot,'stacked')
    legend(legend_prod,'Location','northeast','FontSize',9)%(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat(("log_1_0 NO [mr]"))); ylabel(strcat(budget_for(l), " [mr/s]"));

    subplot(2,1,2);
    hold on; grid on;
    title(strcat("Loss of " , budget_for(l) ," ",(under_after(m))))
    bar(log10(NOmr(2:end)), loss_plot ,'stacked')
    legend(legend_loss,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat(" log_1_0 NO [mr]"));ylabel(strcat(budget_for(l), " [+/-mr]"));
    
    if save_figures == "Yes"
        filename = strcat(budget_for(l),"_budget_",(under_after(m)));
        saveas(gca, fullfile(fname, VOC,"Budget", filename), 'fig');
    end
    
else
     
    figure;
    subplot(2,1,1);
    title(strcat("Production of " , budget_for(l) ," ",(under_after(m))," lamp"))
    hold on; grid on;
    bar(VOCmr(2:end), prod_plot,'stacked')
    legend(legend_prod,'Location','northeast','FontSize',9)%(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat((VOC(v))," [mr]"));ylabel(strcat(budget_for(l), " [+/-mr]"));

    subplot(2,1,2);
    hold on; grid on;
    title(strcat("Loss of " , budget_for(l) ," ",(under_after(m))," lamp"))
    bar(VOCmr(2:end), loss_plot ,'stacked')
    legend(legend_loss,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat((VOC(v))," [mr]"));ylabel(strcat(budget_for(l), " [+/-mr]"));
    
        if save_figures == "Yes"
        filename = strcat(budget_for(l),"_budget_",(under_after(m)));
        saveas(gca, fullfile(Budget_folder, filename), 'fig');
        end
    end
  end
end
hold off
%% Plot Zero-Value OH without VOC
%  for m=1:length (under_after)
% figure;
% subplot(2,1,1);
% title(strcat("Production of OH without VOC"," ",(under_after(m))," lamp"));
% hold on; grid on;
% bar([1,2],prod.(under_after(m)).OHzeroVOC,'stacked')
% legend(budgets.(under_after(m)).OH(1).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
% legend('toggle')
% xlabel(strcat((VOC)," mr = 0"));ylabel(" OH ");
% 
% subplot(2,1,2);
% hold on; grid on;
% title(strcat("Loss of OH without VOC", " ",(under_after(m))," lamp") )
% bar([1,2],loss.(under_after(m)).OHzeroVOC,'stacked')
% legend(budgets.(under_after(m)).OH(1).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
% legend('toggle')
% xlabel(strcat((VOC)," mr = 0"));ylabel(" OH ");
% hold off
% 
%  end
end
