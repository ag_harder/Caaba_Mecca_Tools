%% calculate 3rd order reaction rates from caaba mecca
function [k_3rd_caabamecca]=third_order_reaction_rate(temp,cair,k0_300K,n,kinf_300K,m,fc)

zt_help = 300./temp;
k0_T    = k0_300K.*zt_help.^n.*cair; % k_0   at current T
kinf_T  = kinf_300K.*zt_help.^m; % k_inf at current T
k_ratio = k0_T./kinf_T;
nu      = 0.75-1.27.*log10(fc);
k_3rd_caabamecca = k0_T./(1+k_ratio).*fc.^(1./(1+(log10(k_ratio)./nu).^2));
end
