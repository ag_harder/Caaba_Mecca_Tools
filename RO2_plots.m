clearvars

stargate_path = 'C:\Users\F.Kunkler\Desktop\VM\stargate';
filename_under = 'C2H4_50lmin_15-Oct-2020_under_lamp';
filename_after = 'C2H4_50lmin_15-Oct-2020_after_lamp';

load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_under,'.mat')));
load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_under,'.mat')));

load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_after,'.mat')));
load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_after,'.mat')));
%% Upcomin loop for comparison of the 12 runs
y_variables = ["OH","HOCH2CH2O2"];
x_variables = ["runtime"];
VOC = ["C2H4"];
for l=1:length(y_variables)
    if length(x_variables)==1
        x=x_variables(1);
    elseif length(x_variables)==2
        x=x_variables(l); %L
    end
    for k=2:12
       if k<10
         run = strcat('run000',num2str(k));
       elseif k>=10
         run = strcat('run00',num2str(k));
       elseif k>=100
         run = strcat('run0',num2str(k));
       elseif k>=1000
         run = strcat('run',num2str(k));
        end
y = y_variables(l);
x_u = '[s]';
y_u = '[mr]';

figure (l) %('Name','OH vs runtime','Color','w')
grid on; hold on;
plot ([runs.(run).(x)],[runs.(run).(y_variables(l))],'marker','.','MarkerSize',15)
title((strcat((y_variables(l))," progress at 50 l/min (after lamp) ")))
xlabel(strcat(x," ",x_u)); ylabel(strcat(y," ",y_u));

for_legend(k-1)= strcat("C2H4 mr = ",num2str([output_struct(k).(VOC)]));
    end
legend ( for_legend,'Location','Best','FontSize',7)    

    

end

%% Say what you want to plot

x='OH';
x_u = '[mole/cm^3]';
y='VOC';
y_u = '[mole/cm^3]';

%% OH vs VOC (onyl one VOC)
figure(1)
VOC = 'CH4';
grid on; hold on
scatter ([output_struct(1:4).OH],[output_struct(1:4).CH4],'s','s')
title(['OH progress after the Lamp'])
xlabel([x ' ' x_u]); ylabel([y ' ' y_u])
legend (VOC)
saveas (gcf, strcat([x (' vs ') y ('(afterlamp)')],'.fig'))
hold off

figure(2)
VOC = 'C2H6';
plot ([output_struct(5:8).OH],[output_struct(5:8).C2H6],'Color',[0.4940 0.1840 0.5560],'linestyle','none','marker','*')
title([x (' vs ') y])
xlabel([x ' ' x_u]); ylabel([y ' ' y_u])
legend (VOC)
saveas (gcf, strcat([x (' vs ') VOC],'.fig'))

figure(3)
VOC = 'C3H8';
plot ([output_struct(9:12).OH],[output_struct(9:12).C3H8],'Color',[0.4660 0.6740 0.1880],'linestyle','none','marker','o')
title([x (' vs ') y])
xlabel([x ' ' x_u]); ylabel([y ' ' y_u])
legend (VOC)
saveas (gcf, strcat([x (' vs ') VOC],'.fig'))

%% OH vs RO2 (only one VOC)
x='OH';
x_u = '[mole/cm^3]';
y='RO2';
y_u = '[mole/cm^3]';


figure(4)
VOC = 'CH3O2';
plot ([output_struct(1:4).OH],[output_struct(1:4).CH3O2],'Color','b','linestyle','none','marker','*')
title([x (' vs ') y])
xlabel([x ' ' x_u]); ylabel([y ' ' y_u])
legend ('CH3O2')
saveas (gcf, strcat([x (' vs ') VOC],'.fig'))

figure(5)
VOC='C2H5O2';
plot ([output_struct(5:8).OH],[output_struct(5:8).C2H5O2],'Color',[0.4940 0.1840 0.5560],'linestyle','none','marker','o')
title([x (' vs ') y])
xlabel([x ' ' x_u]); ylabel([y ' ' y_u])
legend ('C2H5O2')
saveas (gcf, strcat([x (' vs ') VOC],'.fig'))

% figure(6)
% plot ([output_struct(9:12).OH],[output_struct(9:12).C3H7O2],'Color',[0.4660 0.6740 0.1880],'linestyle','none','marker','o')
% title([x (' vs ') y])
% xlabel([x ' ' x_u]); ylabel([y ' ' y_u])
% legend ('C3H7O2')
%% PLot of VOC conc vs OH
% 
% figure(1)
% plot ([output_struct(1:4).CH4],[output_struct(1:4).OH])% nur die ersten 4 Fields von OH/CH4


