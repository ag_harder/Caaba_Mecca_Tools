
function [budgets_run] = plot_budget_run(budget_path,filename,runnum,budgetfor1,budgetfor2,budgetfor3)

%plot_budget_runs(budget_path,filename,runnum,budgetfor1,budgetfor2,budgetfor3)
%
%
%function to plot budgets of a single run !TIMERESOLVED! for up to 3 species vs the runtime
%       runnum = Run-number you want to plot e.g. 1
%       budgetforX are the species e.g. OH or CH4..
%
%                       ATTENTION !!!! 
%you may need to config the datapath in the function

load(strcat('C:\Users\Felix\Desktop\Mastersachen\VM\stargate\output_matlab\output_runs_',filename,'.mat'));

budgets_run = struct;
%% Choose Run and what u want 
runnum=strcat('run000',num2str(runnum));

if nargin == 6
    budget_for = [budgetfor1,budgetfor2,budgetfor3];
elseif nargin == 5
    budget_for = [budgetfor1,budgetfor2];
elseif nargin == 4
    budget_for = [budgetfor1];
end

for k=1:length(budget_for) 
    datfile_path(k)=strcat(budget_path,'\budgetUnKnwn',budget_for(k),'.dat');
end
%% get Values for Budgets

for l=1:length(budget_for) 
for k=1:length(runs.(runnum))
    budgets_run.(budget_for(l))(k)= budgetcalc(datfile_path(l),runs.(runnum)(k)); 
end
end
%for OH_budget
% for k=1:length(runs.(runnum))
%     budget_OH(k).(runnum)= budgetcalc(datfile_path(k),runs.(runnum)(k));
% end

%% 
%in ProdRxn Reaktionsgleichungen der Produktion
timevalue = [runs.(runnum).runtime].'; %[] holt das timerun-Field raus

%% in Loss all loss data per timestep same for prod
prod = struct;
for l=1:length(budget_for) 
    if l==1 
        for k=2:length(timevalue)
        prod1(:,(k-1)) = budgets_run.(budget_for(l))(k).Prod;
        loss1(:,(k-1)) = budgets_run.(budget_for(l))(k).Loss;   
        end
        prod1 = prod1.';prod.(budget_for(l)) = prod1;
        loss1 = loss1.';loss.(budget_for(l)) = loss1;
        prod1=[]; loss1=[];
    elseif l==2 
        for k=2:length(timevalue)
        prod2(:,(k-1)) = budgets_run.(budget_for(l))(k).Prod;
        loss2(:,(k-1)) = budgets_run.(budget_for(l))(k).Loss;   
        end
        prod2=prod2.'; prod.(budget_for(l)) = prod2;
        loss2 = loss2.';loss.(budget_for(l)) = loss2;
        prod2=[]; loss2=[];
     elseif l==3 
        for k=2:length(timevalue)
        prod3(:,(k-1)) = budgets_run.(budget_for(l))(k).Prod;
        loss3(:,(k-1)) = budgets_run.(budget_for(l))(k).Loss;   
        end
        prod3=prod3.'; prod.(budget_for(l)) = prod3;
        loss3 = loss3.';loss.(budget_for(l)) = loss3;
        prod3=[]; loss3=[];
    end

end
%% Plot of Loss and Prod
figure;

%if u want all 4 in 1
for l=1:length(budget_for)
if l==1
    subplot(4,1,l);
elseif l==2
    subplot(4,1,3)
end
title(strcat("Production of " , budget_for(l)))
hold on; grid on;
bar(timevalue(2:end), prod.(budget_for(l)) ,'stacked')
legend(budgets_run.(budget_for(l))(4).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel('time [s]');ylabel(strcat(budget_for(l), " [mr]"));
subplot(4,1,2*l);
hold on; grid on;
title(strcat("Loss of " , budget_for(l)))
bar(timevalue(2:end), loss.(budget_for(l)) ,'stacked')
legend(budgets_run.(budget_for(l))(4).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel('time [s]');ylabel(strcat(budget_for(l), " [mr]"));
end
hold off

for l=1:length(budget_for)
figure;
subplot(2,1,1);
title(strcat("Production of " , budget_for(l)," (",runnum,")"))
hold on; grid on;
bar(timevalue(2:end), prod.(budget_for(l)) ,'stacked')
legend(budgets_run.(budget_for(l))(4).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel('time [s]');ylabel(strcat(budget_for(l), " "));

subplot(2,1,2);
hold on; grid on;
title(strcat("Loss of " , budget_for(l)," (",runnum,")"))
bar(timevalue(2:end), loss.(budget_for(l)) ,'stacked')
legend(budgets_run.(budget_for(l))(4).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel('time [s]');ylabel(strcat(budget_for(l), " "));
end
hold off
end