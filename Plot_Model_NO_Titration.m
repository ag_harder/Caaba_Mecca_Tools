% Plot NO Titration
% clearvars
Config_at_userpath;
% stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';
plot_colors = ['#0072BD';'#FF0000';'#77AC30';'#EDB120';'#4DBEEE';'#7E2F8E';'#D95319';'#FFC1C1';'#FF1493';'#FFD700';'#C1CDC1'];
plot_marker = ['x';'*';'o';'^';'.';'s';'d';'v';'+';'x';'*'];
clear filename

VOC_use = true;
VOC = "C3H8";
if VOC_use == true
    if VOC == "CH4"
        filename = 'Zero_and_Mixed_VOC_25-May-2021_intern_runnum_2_modified';
        RO2 = ["CH3O2"];
        EndProducts = ["HCHO"];
        RO2x = ["CH3O2x"];
    elseif VOC =="C2H4"
       filename = 'Zero_and_Mixed_VOC_25-May-2021_intern_runnum_3_modified';
       RO2 = ["HOCH2CH2O2"];
       EndProducts = ["HCHO"]; % Eher Schlecht, da RO Disproportioniert ZU 2xFormaldehyd + HO2 schwer in allgemeiner Form zu berücksichtigen...
      RO2x = ["HOCH2CH2O2x"];
    elseif VOC =="C3H6"
       filename = 'Zero_and_Mixed_VOC_25-May-2021_intern_runnum_4_modified';
       RO2 = "HYPROPO2"; RO2x = ["HYPROPO2x"];
    elseif VOC =="C5H8"
       filename = 'Zero_and_Mixed_VOC_25-May-2021_intern_runnum_5_modified';
       RO2 = ["ISOPDO2","ISOPBO2","LISOPACO2","LDISOPACO2","LISOPEFO2","DB1O2","DB2O2"];
       EndProducts = ["ME3FURAN","MVK","MACR","DB1O"];
       RO2x = ["ISOPRENRO2x"];
     elseif VOC =="C3H8"
       filename = 'Zero_and_Mixed_VOC_05-Jul-2021_intern_runnum_6_modified';
       RO2 = ["IC3H7O2","NC3H7O2"];
       EndProducts = ["CH3COCH3","C2H5CHO"];
       RO2x = ["IC3H7O2x","NC3H7O2x"];
     elseif VOC =="BENZENE"
       filename = 'Zero_and_Mixed_VOC_28-Jun-2021_intern_runnum_7_modified';
       RO2 = ["BZBIPERO2","BZEPOXMUC"];
       EndProducts = ["GLYOX","BZFUONE"];
       RO2x = ["BZBIPERO2x","BZEPOXMUCx","PHENOLx"];
    end
else 
    filename = 'Zero_and_Mixed_VOC_25-May-2021_intern_runnum_1_modified';
end


C5H8 = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_Zero_and_Mixed_VOC_25-May-2021_intern_runnum_5_modified.mat')));
C3H6 = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_Zero_and_Mixed_VOC_25-May-2021_intern_runnum_4_modified.mat')));
C2H4 =  load(fullfile(stargate_path,'output_matlab',strcat('output_runs_Zero_and_Mixed_VOC_25-May-2021_intern_runnum_3_modified.mat')));
CH4 = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_Zero_and_Mixed_VOC_25-May-2021_intern_runnum_2_modified.mat')));
Zero = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_Zero_and_Mixed_VOC_25-May-2021_intern_runnum_1_modified.mat')));
C3H8 = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_Zero_and_Mixed_VOC_24-Jun-2021_intern_runnum_6_modified_RO_2.mat')));
load(fullfile(stargate_path,'output_matlab',strcat('output_',filename,'.mat')));
load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename,'.mat')));

model_Temp = [output_struct.nml_temp].'; %K
model_press = ([output_struct.nml_press]/100).'; %mbar
Loschmidt = 2.686780111*1E+19; %1/cc
ppt_to_density = 1E-12*(Loschmidt*(model_press(1)/1013)*(273.15/model_Temp(1)));


Pressure_mean = mean(p20_ax1_plot,1); %mbar
int_temp_mean = mean(int_temp_onl_plot,1); %K
k_HO2 = 3.3E-12*exp(270./int_temp_mean);
exp_time_Model = 9.9; %ms

int_flow = (7.2+0.92+0.2)*1000*(1013.15/Pressure_mean)*(int_temp_mean/273.15); %sccm   % 2382000 sccm
% int_flow = 2382000; %sccm   %  sccm

conditionfactor = (model_press./Pressure_mean).*(int_temp_mean./model_Temp);






for k=1:1 %length(int_flow)

    
NO_bottle_mr = 0.253;   %0.253

NO_mr = [output_struct.NO].';
NO_sccm = ((NO_mr*int_flow(k))./(NO_bottle_mr.*((1013./model_press).*(model_Temp./273.15)))).*conditionfactor;
OH_mr = [output_struct.OH].'; OH_ppt = OH_mr.*1E+12;
HO2_mr = [output_struct.HO2].';HO2_ppt = HO2_mr.*1E+12; HO2_red = 1-(HO2_ppt./HO2_ppt(1));
% figure (5)
% plot(NO_sccm(1:17),(HO2_ppt(1)-HO2_ppt(1:17)),'ks','MarkerSize',12)
% grid on;hold on;
% 
% plot(NO_sccm(1:17),(OH_ppt(1:17)-HO2_ppt(1)),'k+','MarkerSize',12)
% grid on; hold on

end

%legend("Mean of NO Titrations", "HO2 Reduction (Model data at 39.7 L/min)", "OH mr (Model data at 39.7 L/min)","HO2 Reduction (Model data at 56.4 L/min)", "OH mr (Model data at 56.4 L/min)")
NO_bp = [NO_sccm(4);NO_sccm(5:10);NO_sccm(11:15)];
HO2 = (OH_ppt(1:17));
HO2_bp=[HO2(4);HO2(5:10);HO2(11:15)];
NO_bp_mess = [0.499447323728140;0.798712193939394;0.998836614379085;1.49800688430691;1.99752975986526;2.99628507753532;4.99432445665635;10.0049737644072;15.0089442698512;20.0123163626801;25.0159935760456;30.0193300073701;40.0286587145970;50.0367197293447;55.0398964653165];
HO2_model_interp=interp1(NO_bp,HO2_bp,NO_bp_mess);

model_flow = repmat(200,length(HO2_model_interp),1);

% figure (11)
% grid on;hold on;
% plot (model_flow,HO2_model_interp,'Color','k','Marker','x','MarkerSize',12,'LineStyle','none')


%% Plot Different Reactiontimes

runtimes = [runs.run0001.runtime]; %ms
runs_table_HO2 = NaN(width(struct2table(runs)),length(runtimes));
runs_table_HO2(:,1)=NO_sccm;
runs_table_OH = NaN(width(struct2table(runs)),length(runtimes));
runs_table_OH(:,1)=NO_sccm;
runs_table_HONO = NaN(width(struct2table(runs)),length(runtimes));
runs_table_HONO(:,1)=NO_sccm;
runs_table_RO2 = NaN(width(struct2table(runs)),length(runtimes));
runs_table_RO2(:,1)=NO_sccm;
runs_table_EndProducts = NaN(width(struct2table(runs)),length(runtimes));
runs_table_EndProducts(:,1)=NO_sccm;
runs_table_HO2x = NaN(width(struct2table(runs)),length(runtimes));
runs_table_HO2x(:,1)=NO_sccm;
runs_table_OHx = NaN(width(struct2table(runs)),length(runtimes));
runs_table_OHx(:,1)=NO_sccm;
runs_table_RO2x = NaN(width(struct2table(runs)),length(runtimes));
runs_table_RO2x(:,1)=NO_sccm;
timesteps = 0.2; % ms
runtime = 8.2; %ms
% (acttime/timesteps)+2;

for k=1:width(struct2table(runs))
    if k >= 10
        runnum = strcat("run00",num2str(k));
    elseif k<10
        runnum = strcat("run000",num2str(k));
    end

    for j=1:length(runtimes)
        runs_table_HO2(k,j+1)=runs.(runnum)(j).HO2;
        runs_table_OH(k,j+1)=runs.(runnum)(j).OH;
        try
        runs_table_HONO(k,j+1)=runs.(runnum)(j).HONO;
        catch
        runs_table_HONO(k,j+1)= 0;    
        end
        try
        runs_table_HO2x(k,j+1)=runs.(runnum)(j).HO2x;
        catch
        runs_table_HO2x(k,j+1)= 0;    
        end
        try
        runs_table_OHx(k,j+1)=runs.(runnum)(j).OHx;
        catch
        runs_table_OHx(k,j+1)= 0;    
        end
        try
            if length(RO2x)>1
                 sum_RO2x = 0;
            for p=1:length(RO2x)
                sum_RO2x = sum_RO2x + runs.(runnum)(j).(RO2x(p));
            end
            runs_table_RO2x(k,j+1)=sum_RO2x;
            else
                runs_table_RO2x(k,j+1)=runs.(runnum)(j).(RO2x);
            end
        catch
            runs_table_RO2x(k,j+1)= 0;    
        end
        try
        if length(RO2)>1
            sum_RO2 = 0;
            for p=1:length(RO2)
                sum_RO2 = sum_RO2 + runs.(runnum)(j).(RO2(p));
            end
        runs_table_RO2(k,j+1)=sum_RO2;
        else
            runs_table_RO2(k,j+1)=runs.(runnum)(j).(RO2);
        end
        catch
        runs_table_RO2(k,j+1)= 0;    
        end
        try
        if length(EndProducts)>1
            sum_EndProducts = 0;
            for p=1:length(EndProducts)
                sum_EndProducts = sum_EndProducts + runs.(runnum)(j).(EndProducts(p));
            end
        runs_table_EndProducts(k,j+1)=sum_EndProducts;
        else
            runs_table_EndProducts(k,j+1)=runs.(runnum)(j).(EndProducts);
        end
        catch
        runs_table_EndProducts(k,j+1)= 0;    
        end
    end
end

% Fit for 2 Exponential fitcurve for the two different reactiontimes
fittimes = [0.00748;0.001357]; %ms
fit_koef_factors = [0.95;0.05];

for k=1:height(runs_table_HO2)
    
   fit_OH(k,:)= interp1(runtimes.',runs_table_OH(k,2:end),fittimes);
   fit_HO2(k,:)= interp1(runtimes.',runs_table_HO2(k,2:end),fittimes);
   try
   fit_HONO(k,:)= interp1(runtimes.',runs_table_HONO(k,2:end),fittimes);
   end
   try
   fit_RO2(k,:)= interp1(runtimes.',runs_table_RO2(k,2:end),fittimes);
   end
   try
   fit_EndProducts(k,:)= interp1(runtimes.',runs_table_EndProducts(k,2:end),fittimes);
   end
   try
   fit_HO2x(k,:)= interp1(runtimes.',runs_table_HO2x(k,2:end),fittimes);
      end
   try
   fit_OHx(k,:)= interp1(runtimes.',runs_table_OHx(k,2:end),fittimes);
   end
   try
   fit_RO2x(k,:)= interp1(runtimes.',runs_table_RO2x(k,2:end),fittimes);
   end
end

OH_fit = fit_OH(:,1)*fit_koef_factors(1)+fit_OH(:,2)*fit_koef_factors(2);
HO2_fit = fit_HO2(:,1)*fit_koef_factors(1)+fit_HO2(:,2)*fit_koef_factors(2);
try
HONO_fit = fit_HONO(:,1)*fit_koef_factors(1)+fit_HONO(:,2)*fit_koef_factors(2);
end
try
RO2_fit = fit_RO2(:,1)*fit_koef_factors(1)+fit_RO2(:,2)*fit_koef_factors(2);
end
try
EndProducts_fit = fit_EndProducts(:,1)*fit_koef_factors(1)+fit_EndProducts(:,2)*fit_koef_factors(2);
end
try
HO2x_fit = fit_HO2x(:,1)*fit_koef_factors(1)+fit_HO2x(:,2)*fit_koef_factors(2);
end
try
OHx_fit = fit_OHx(:,1)*fit_koef_factors(1)+fit_OHx(:,2)*fit_koef_factors(2);
end
try
RO2x_fit = fit_RO2x(:,1)*fit_koef_factors(1)+fit_RO2x(:,2)*fit_koef_factors(2);
end
try
    if VOC_use == true
        exp_time_corfactor = mean(exp_time_VOC)/exp_time_Model;
    else
        exp_time_corfactor = mean(exp_time_H2O)/exp_time_Model;
    end
    disp(strcat("!!!!!!! Used  calculated exposuretime of ",num2str(mean(exp_time_VOC))," ms !!!!!!!"))
catch
    exp_time_corfactor = 9.7780/9.9;
    disp("!!!!!!! Used default exposuretime of 9.78 ms !!!!!!!")
end

NO = runs_table_HO2(:,1);
OH_HO2 = runs_table_HO2(1,39)-runs_table_HO2(:,39);
OH = runs_table_OH(:,39);

%     figure (2)
%     grid on; hold on;
%     plot (NO(1:17),(OHx_fit(1:17)).*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;  %OH Signal - OH aus HO2

    
    figure (3)
    grid on; hold on;
    plot (NO(1:17),(OHx_fit(1:17)+OH_fit(1:17)+HONO_fit(1:17)).*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;  %OH Signal - OH aus HO2

    
    figure (4)
    grid on; hold on;
    plot (NO(1:17),(OHx_fit(1:17)).*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;  %OH Signal - OH aus HO2

    Rest_RO2 = [RO2x_fit(17), output_struct(17).HONOx,output_struct(17).HO2x];
    
    figure 
    c=1;
    grid on; hold on;
    title(strcat("Model Verläufe relevanter Moleküle für ",VOC))
    plot(NO(1:17),RO2_fit(1:17).*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    plot(NO(1:17),HO2x_fit(1:17).*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    plot(NO(1:17),OHx_fit(1:17).*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    plot(NO(1:17),OH_fit(1:17).*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    plot(NO(1:17),RO2x_fit(1:17).*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    
    xlabel("NO [sccm]"); ylabel("Konzentration [ppt]");
    legend("RO2","HO2x","OHx auf HO2x","OH aus HO2","RO2x aus OHx",'Location','best')
    
    figure
    c=1;
    grid on; hold on;
    title(strcat("HO2 Signal Anteile bei ",VOC))
    plot(NO(1:17),(OH_fit(1:17).*1E12*exp_time_corfactor)+(OHx_fit(1:17).*1E12*exp_time_corfactor),'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    plot(NO(1:17),OH_fit(1:17).*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    plot(NO(1:17),OHx_fit(1:17).*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    xlabel("NO [sccm]"); ylabel("Konzentration [ppt]");
    legend("Summensignal HO2","HO2","HO2x",'Location','best')
    
    figure
    c=1;
    grid on; hold on;
    title(strcat("HO2 Signal relative Anteile bei ",VOC))
    plot(NO(1:17),(((OH_fit(1:17).*1E12*exp_time_corfactor)+(OHx_fit(1:17).*1E12*exp_time_corfactor))./((OH_fit(1:17).*1E12*exp_time_corfactor)+(OHx_fit(1:17).*1E12*exp_time_corfactor))),'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    plot(NO(1:17),((OH_fit(1:17).*1E12*exp_time_corfactor)./((OH_fit(1:17).*1E12*exp_time_corfactor)+(OHx_fit(1:17).*1E12*exp_time_corfactor))),'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    plot(NO(1:17),((OHx_fit(1:17).*1E12*exp_time_corfactor)./((OH_fit(1:17).*1E12*exp_time_corfactor)+(OHx_fit(1:17).*1E12*exp_time_corfactor))),'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
    xlabel("NO [sccm]"); ylabel("");
    legend("Summensignal HO2","HO2","HO2x",'Location','best')
    % SUmme von HO2x/ HOx und HO2 bzw OH Anteilig wieviel von was kommt. 
    
    
    
%     plot (NO(1:16),OH_HO2(1:16)*1E12*exp_time_corfactor,'Color','k','Marker','x','MarkerSize',12); c=c+1;
%     plot (NO(1:16),(OH_fit(1:16)+HONO_fit(1:16)-OH_fit(1))*1E12*exp_time_corfactor,'Color','k','Marker','x','MarkerSize',12); c=c+1;
%     plot (NO(1:16),(OH_fit(1:16)+HONO_fit(1:16)-OH_fit(1))*1E12*exp_time_corfactor,'Color',plot_colors(c,:),'Marker',plot_marker(c,:)); c=c+1;
% 
%     errorbar(mean(NO_values_H2O,2),mean(HO2_values_cor_NO,2),mean(HO2_values_H2O_err,2),'Color',plot_color(c,:)); c=c+1;
%     errorbar(mean(NO_values_VOC,2),mean(HO2_values_cor_VOC,2),mean(HO2_values_VOC_err,2),'Color',plot_color(c,:));c=c+1;
%        title("Modeldata comparison with measurement (corrected for exposuretime & without HONO formation)")
%        legend("interpolated Modeldata without VOC (corrected for exposuretime)","interpolated Modeldata with VOC (corrected for exposuretime)","Measurement from 20.05.2021 without CH4","Measurement from 20.05.2021 with CH4")
%        ylabel("HO2 [ppt]");xlabel("NO [sccm]");
p=1;
figure
grid on; hold on;
% plot (NO(1:16),OH_HO2(1:16),'b*')
plot (NO(1:16),(OH(1:16)-OH(1))*1E12,'Color',plot_colors(p,:),'Marker',plot_marker(p,:)); p=p+1;
plot (NO(1:16),(OH_fit(1:16)-OH_fit(1))*1E12,'Color',plot_colors(p,:),'Marker',plot_marker(p,:)); p=p+1;
plot (NO(1:16),(OH_fit(1:16)+HONO_fit(1:16)-OH_fit(1))*1E12,'Color',plot_colors(p,:),'Marker',plot_marker(p,:)); p=p+1;
if VOC_use == true
    plot(mean(NO_values_VOC,2),mean(HO2_values_cor_VOC,2),'kd','LineStyle','-')
    legend("Pure model data","interpolated Modeldata for two reactiontimes (1.4ms and 7.5 ms)","interpolated Modeldata with complete HONO photolysis (two reactiontimes)","Measurement from 20.05.2021 with CH4")
    ylabel("HO2 [ppt]");
else
    plot(mean(NO_values_H2O,2),mean(HO2_values_cor_NO,2),'kd','LineStyle','-')
    legend("Pure model data","interpolated Modeldata for two reactiontimes (1.4ms and 7.5 ms)","interpolated Modeldata with complete HONO photolysis (two reactiontimes)","Measurement from 20.05.2021 without VOC")
end
xlabel("NO [sccm]"); ylabel("HO2 [ppt]");
title("Modeldata comparison with different approaches")

% with corrected exposuretime
p=1;
figure
grid on; hold on;
% plot (NO(1:16),OH_HO2(1:16),'b*')
plot (NO(1:16),(OH(1:16)-OH(1))*1E12*exp_time_corfactor,'Color',plot_colors(p,:),'Marker',plot_marker(p,:)); p=p+1;
plot (NO(1:16),(OH_fit(1:16)-OH_fit(1))*1E12*exp_time_corfactor,'Color',plot_colors(p,:),'Marker',plot_marker(p,:)); p=p+1;
plot (NO(1:16),(OH_fit(1:16)+HONO_fit(1:16)-OH_fit(1))*1E12*exp_time_corfactor,'Color',plot_colors(p,:),'Marker',plot_marker(p,:)); p=p+1;
if VOC_use == true
    plot(mean(NO_values_VOC,2),mean(HO2_values_cor_VOC,2),'kd','LineStyle','-')
    legend("Pure model data","interpolated Modeldata for two reactiontimes (1.4ms and 7.5 ms)","interpolated Modeldata with complete HONO photolysis (two reactiontimes)","Measurement from 20.05.2021 with CH4")
    ylabel("HO2 [ppt]");
else
    plot(mean(NO_values_H2O,2),mean(HO2_values_cor_NO,2),'kd','LineStyle','-')
    legend("Pure model data","interpolated Modeldata for two reactiontimes (1.4ms and 7.5 ms)","interpolated Modeldata with complete HONO photolysis (two reactiontimes)","Measurement from 20.05.2021 without VOC")
    ylabel("HO2 [ppt]");
end
xlabel("NO [sccm]"); 
title("Modeldata (corrected for exposure time) comparison with different approaches")

%Figure mit Summensignal OH nicht nur OH aus HO2
figure
hold on;grid on;
clear legenden_data_model
for k=17:width(runs_table_OH)-4 %3 because 1 is NO sccm and 2 is 0 ms reaction time
   y_plot =((runs_table_OH(1:end-4,k)))*1E12;
   
   if timesteps*(k-2) >=6
       color = '#a11b72';
   elseif timesteps*(k-2) >=5
       color = '#4DBEEE';
   elseif timesteps*(k-2) >=4
       color = 'r';
   elseif timesteps*(k-2) >=3
       color = 'g';
   elseif timesteps*(k-2) >=2
       color = 'b';
   else
       color = 'k';
   end
    plot(runs_table_OH(1:end-4,1),y_plot,'Color',color); 
    legenden_data_model(k-16) = strcat("Reactiontime of ",num2str(timesteps*(k-2))," ms");
end
legend(legenden_data_model)
xlabel("NO [sccm]");ylabel("OH [mr]")
title("OH Verlauf NO Titration")

% FIgure Summensignal OH nicht nur OH aus HO2 + Keine HONO Bildung 
figure
hold on;grid on;
clear legenden_data_model
for k=17:width(runs_table_OH)-4 %3 because 1 is NO sccm and 2 is 0 ms reaction time
   y_plot =((runs_table_OH(1:end-4,k))+(runs_table_HONO(1:end-4,k)))*1E12;
   
   if timesteps*(k-2) >=6
       color = '#a11b72';
   elseif timesteps*(k-2) >=5
       color = '#4DBEEE';
   elseif timesteps*(k-2) >=4
       color = 'r';
   elseif timesteps*(k-2) >=3
       color = 'g';
   elseif timesteps*(k-2) >=2
       color = 'b';
   else
       color = 'k';
   end
    plot(runs_table_OH(1:end-4,1),y_plot,'Color',color); 
    legenden_data_model(k-16) = strcat("Reactiontime of ",num2str(timesteps*(k-2))," ms");
end
legend(legenden_data_model)
xlabel("NO [sccm]");ylabel("OH [mr]")
title("OH Verlauf NO Titration")

%Figure HO2 Signalverlauf 
figure
hold on;grid on;
clear legenden_data_model
for k=17:width(runs_table_OH)-4 %3 because 1 is NO sccm and 2 is 0 ms reaction time
   y_plot =((runs_table_OH(1:end-4,k))-(runs_table_OH(1,k)))*1E12;
   
   if timesteps*(k-2) >=6
       color = '#a11b72';
   elseif timesteps*(k-2) >=5
       color = '#4DBEEE';
   elseif timesteps*(k-2) >=4
       color = 'r';
   elseif timesteps*(k-2) >=3
       color = 'g';
   elseif timesteps*(k-2) >=2
       color = 'b';
   else
       color = 'k';
   end
    plot(runs_table_OH(1:end-4,1),y_plot,'Color',color); 
    legenden_data_model(k-16) = strcat("Reactiontime of ",num2str(timesteps*(k-2))," ms");
end
legend(legenden_data_model)
xlabel("NO [sccm]");ylabel("OH [ppt]")
title("OH aus HO2 Verlauf NO Titration")
% figure(1)
% hold on;grid on;
% clear legenden_data_model
% for k=3:width(runs_table) %3 because 1 is NO sccm and 2 is 0 ms reaction time
%    y_plot =(((runs_table(1:end-4,k)-runs_table(1,k))./(max(max(runs_table(1:end-4,2:end)))-runs_table(1,k)))*82.15)*ppt_to_density;
%    
%     if timesteps*(k-2) >=5
%        color = '#4DBEEE';
%    elseif timesteps*(k-2) >=4
%        color = 'r';
%    elseif timesteps*(k-2) >=3
%        color = 'g';
%    elseif timesteps*(k-2) >=2
%        color = 'b';
%    else
%        color = 'k';
%     end
%    
%     plot(NO_mr(1:end-4,1)*1E+12*ppt_to_density,y_plot,'Color',color); 
%     legenden_data_model(k-2) = strcat("Reactiontime of ",num2str(timesteps*(k-2))," ms");
% end
% 
% legend(legenden_data_model)
% title ("Model verschiedener Reaktionszeiten von NO mit HO2")
% xlabel("NO density");ylabel("HO2 density");



figure
hold on;grid on;
clear legenden_data_model
for k=17:width(runs_table_OH) %3 because 1 is NO sccm and 2 is 0 ms reaction time
   y_plot =((runs_table_OH(1:end-4,k)-runs_table_OH(1,k)))*1E12;
   
   if timesteps*(k-2) >=6
       color = '#a11b72';
   elseif timesteps*(k-2) >=5
       color = '#4DBEEE';
   elseif timesteps*(k-2) >=4
       color = 'r';
   elseif timesteps*(k-2) >=3
       color = 'g';
   elseif timesteps*(k-2) >=2
       color = 'b';
   else
       color = 'k';
   end
    plot(runs_table_HO2(1:end-4,1),y_plot,'Color',color); 
    legenden_data_model(k-16) = strcat("Reactiontime of ",num2str(timesteps*(k-2))," ms");
end
plot(mean(NO_values,2),mean(HO2_values_cor,2),'Color','k','LineWidth',2)
% plot(NO_values,HO2_values_cor)
legend(legenden_data_model)
title ({["Model verschiedener Reaktionszeiten von NO mit HO2"] ["Mit gemittelten NO Titrationen bei 200 sccm"]})
xlabel("NO [sccm]");ylabel("HO2 ppt");
% figure(6)
% hold on;grid on;
% for k=1:16
%    
%     plot(1:6,(runs_table(k,3:8)-runs_table(1,2))./(max(max(runs_table(1:end-4,2:end)))-runs_table(1,2))*85.74,'Marker','o','MarkerSize',10);  
%     xlabel("Reactiontime [ms]"); ylabel("HO2 [ppt]");
%     legend_value(k)= strcat(num2str(round(runs_table(k,1),1))," sccm NO");
% end
% legend(legend_value)
% plot(repmat(3.5,length(NO_50_sig_mean),1),NO_50_sig_mean,'Marker','^')