clearvars;
stargate_path='C:\Users\F.Kunkler\Desktop\2020_RO2_HO2\stargate\';
stargate_path='C:\Users\f.kunkler\Documents\Seafile\2020_RO2_HO2\stargate\';
%constants
p0=101325 ; t0=298.15; kB=1.380649E-23; % Pa, K, J/K
lozahl=(p0/(t0*kB)/1000000); %1/cm^3
at=[];lt=[];OH_prod=[];


lamp_flow = 1.45E13;
wq_H2O = 7.2E-20;
press = 101325; %Pa
temp=298.15; %K
volume_flow = 50000; %ml/min
j_H2O = 8.93E-7; %

%%Choose VOC
var_name_VOC=["C3H6"];
H2Opercent = 1;
H2Oin = (lozahl*H2Opercent/100)/lozahl;
CH4 = 0;
CO2 = 0;
% CO = 0;

volume_flow = 50000; %sccm/min
percant = 0.01; %how many OH should be left, needed for the calculation of the lifetime 
prod_OH = lamp_flow*wq_H2O*H2Opercent*lozahl; %[1/s]


%for schleife
for k=1:length(var_name_VOC)
  if strcmp(var_name_VOC(k),'CH4') %Methan 
    kVOC= 1.85E-20*exp(2.82*log(temp)-987./temp);
  elseif strcmp(var_name_VOC(k),'C2H6') %Ethan
    kVOC= 1.49E-17*temp*temp*exp(-499./temp);
  elseif strcmp(var_name_VOC(k),'C3H8') %Propan
    kVOC1= 4.50E-18*temp*temp*exp(253./temp); %Isoprop
    kVOC2= 2*4.49E-18*temp*temp*exp(-320./temp); %n-prop
    kVOC = kVOC1+kVOC2;
  elseif strcmp(var_name_VOC(k),'C2H4'); %Ethen
    kVOC= third_order_reaction_rate(temp,lozahl,8.6E-29,3.1,9.E-12,0.85,0.48);
  elseif strcmp(var_name_VOC(k),'C3H6') %Propen
    kVOC= third_order_reaction_rate(temp,lozahl,8.6E-27,3.5,3.E-11,1.,0.5);
  elseif strcmp(var_name_VOC(k),'BENZENE') %Benzol
    kVOC= 2.3E-12*exp(-190/temp);
  elseif strcmp(var_name_VOC(k),'TOLUENE') %Toluol
    kVOC= 1.8E-12*exp(340/temp);
  elseif strcmp(var_name_VOC(k),'C5H8') %Isopren
    kVOC=2.7E-11*exp(390./temp);
  elseif strcmp(var_name_VOC(k),'APINENE') %a-Pinen
    kVOC=1.2E-11*exp(440./temp);    
  elseif strcmp(var_name_VOC(k),'LIMONENE') %Limonen
    kVOC=1;
  elseif strcmp(var_name_VOC(k),'BPINENE') %beta-Pinen
      kVOC=1.47E-11*EXP(467./temp)*(0.8326*0.3+0.068)/(0.8326+0.068);
  elseif strcmp(var_name_VOC(k),'BUT1ENE') %Buten (1-en)
      6.6E-12*exp(465./temp)
  else 
      kVOC=1
  end
  
end


lamp_length = 3.5; channel_width = 1.6; channel_height = 1.6; after_length = 1.18;    %all cm
cs_channel = channel_height*channel_width; %cm^2

flow_speed= ((volume_flow/60)/cs_channel) *(temp/273.15)*(101325/press); %cm/s
lamp_time = lamp_length/flow_speed; %s
after_time= after_length/flow_speed; %s 

concVOC=(log(percant/1)/(-kVOC*after_time));
VOCa = concVOC/lozahl;
%für BENZENE testweise Sehr Hohe MR um BZBIPERO2 zu pushen!
% VOCmr = [0,VOCa,2*VOCa,4*VOCa,6*VOCa,8*VOCa,12*VOCa,16*VOCa,20*VOCa,30*VOCa,40*VOCa];
VOCmr = [0,0.1*VOCa,0.2*VOCa,0.5*VOCa,0.8*VOCa,0.9*VOCa,VOCa,1.1*VOCa,1.2*VOCa,1.5*VOCa,8*VOCa,10*VOCa];

at=[at,after_time];
lt=[lt,lamp_time];
OH=prod_OH*lamp_time;
OH_prod =[OH_prod,OH];




%% input test values
CO =0;
CH4 = 0;
% COmr = [0,0.001,0.002,0.004,0.006,0.008,0.01,0.012,0.016,0.02,0.03];

CH4 = [0,5.7E-3,0,0,0,0,0,0,0]; %2
C2H4 = [0,0,7.83E-6,0,0,0,0,0,0]; %3
C3H6 = [0,0,0,1.97E-6,0,0,0,0,0]; %4
C5H8 = [0,0,0,0,5.83E-7,0,0,0,0]; %5
C3H8 = [0,0,0,0,0,4.7E-5,0,0,0]; %6
BENZENE = [0,0,0,0,0,0,4.7E-5,0,0]; %7
NC4H10 = [0,0,0,0,0,0,0,2.1360E-05,0]; %(1.78/1000)*(600/50000)
BUT1ENE = [0,0,0,0,0,0,0,0,1.8600e-06]; %0.155/1000*(600/50000)
% 1.78 Promille n-butan
% 155 ppm But-1-en

length_array=length(C2H4);

model_input.nml_press=repmat(press,[1,length_array]); %Pa standard
model_input.nml_temp=repmat(temp,[1,length_array]); %K  standard
model_input.H2O=repmat(H2Oin,[1,length_array]); % 1% H2O 
model_input.j_H2O=repmat(j_H2O.*(1/1.3786),[1,length_array]); % J x sigma
model_input.Tgps=repmat(1:length_array,[1,1]);
model_input.CO2=repmat(CO2,[1,length_array]);
model_input.CH4=CH4;
% model_input.(var_name_VOC)=COmr
model_input.CO=repmat(CO,[1,length_array]);
model_input.C2H4=C2H4;
model_input.C3H6=C3H6;
model_input.C5H8=C5H8;
model_input.C3H8=C3H8;
model_input.BENZENE=BENZENE;
model_input.NC4H10=NC4H10;
model_input.BUT1ENE=BUT1ENE;
% for k=1:length(var_name_VOC)
%     VOCin=zeros(1,length(VOCmr2));
%     VOCin(k*length(VOCmr)-(length(VOCmr)-1):k*length(VOCmr))=VOCmr2(k*length(VOCmr)-(length(VOCmr)-1):k*length(VOCmr));
%     model_input.(var_name_VOC(k))=VOCin;
% end


% runtime_str= "'0.001 seconds'";
% timesteplen_str= "'0.0001 seconds'";
% model_input.runtime_str=str2double(repmat(runtime_str,[1,length_array]));
% model_input.timesteplen_str=str2double(repmat(timesteplen_str,[1,length_array]));

%% Create netCDF file from MatLab dataset

if j_H2O > 0
%     fname    = convertStringsToChars(strcat(var_name_VOC(1),'_',num2str(volume_flow/1000),'lmin_',date,'_under_lamp.nc'));
    fname    = convertStringsToChars(strcat('2021_Mixed_VOC_',date,'_under_lamp_new_J.nc'));
elseif  j_H2O ==0
    fname    = 'RO2_Felix_Multi_after_lamp.nc';
end

varnames=fields(model_input);

if exist([stargate_path,fname]) %#ok<EXIST>
    fclose('all');
    clear mex %#ok<CLMEX>
    delete([stargate_path,fname]);
end

ncid     = netcdf.create([stargate_path,fname],'NC_NOCLOBBER'); % Open netCDF file.
len_data = [size(model_input.Tgps,2),size(varnames,1)];   % Define the dimensions of the variable
%dim      = len_data(1)+1;
%dimid    = netcdf.defDim(ncid,'T_AX',len_data);
dimid    = netcdf.defDim(ncid,'time',netcdf.getConstant('NC_UNLIMITED'));

for i = 1 : len_data(2)                          % Define a new variable in the file.
    varID   = netcdf.defVar(ncid,varnames{i},'double',dimid);
    netcdf.endDef(ncid);                    % Leave define mode and enter data mode to write data.
    netcdf.putVar(ncid,varID,0,len_data(1), eval(['model_input.',varnames{i}]));      % Write data to variable.
    netcdf.reDef(ncid);                     % Leave data mode and re-enter define mode
    % netcdf.putAtt(ncid,varID,'units','mixing ratio'); % define attribute for missing value
end
netcdf.close(ncid);

