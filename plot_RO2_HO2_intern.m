clearvars
stargate_path = 'C:\Users\F.Kunkler\Desktop\VM\stargate';
VOC = ["C2H4"];

save_figures = ["Yes"];
    if save_figures == "Yes" 
    fname = 'C:\Users\F.Kunkler\Desktop\VM\Figures';
    VOC_folder = fullfile(fname,VOC);
     
        if ~exist(VOC_folder, 'dir')  %creates the save folders
            mkdir(VOC_folder)
        end
    end

switch VOC
    case "Dummy"
        RO2 = [];
        side_products = [];  
        intern_RO2_product = [];   % 
        intern_RO2_side =[]; % Preproducts..
    case "CH4"
        RO2 = ["CH3O2","CH3O"];
        side_products = ["CH3OOH","HCHO"];
    case "C2H4"
        RO2 = ["HOCH2CH2O2"];
        side_products = ["HOCH2CH2O","HOCH2CHO","ETHGLY","HYETHO2H"];
    case "BENZENE"
        RO2 = ["BZBIPERO2"];
        side_products = ["PHENOL","BZEPOXMUC","CATECHOL","BZEMUCO2","BZEMUCCO3"];
    case "C5H8"
        RO2 = ["LISOPACO2","ISOPBO2","LDISOPACO2","ISOPDO2","LISOPEFO2"];
        side_products = ["LISOPAB","LISOPAB","C1ODC2O2C4OOH","LZCODC23DBCOOH","C1OOHC3O2C4OD","LISOPACOOH"];
    case "C3H6"
        RO2 = ["HYPROPO2"];
        side_products = ["HYPROPO2H","PROPOLNO3"];
    case "APINENE"
        RO2 = ["LAPINABO2","ROO6R1O2","OHMENTHEN6ONEO2","PINALO2"];
        side_products = ["MENTHEN6ONE","LAPINABOOH","PINAL","PINALOOH"];  
        intern_RO2_product = ["PINAL","C106O2","OH2MENTHEN6ONE"];   % Pinal product of LAPINABO2+NO, C106O2 product of PINAL+NO
        intern_RO2_side =["LAPINABNO3","PINALNO3"]; % Preproducts..
end



for k=1:length(VOC)
Voc= load(fullfile(stargate_path,'output_matlab',strcat('output_all_', VOC(1) ,'.mat')));

NOmr = [Voc.VM_all.output_struct.intern.NO];
RO2_ratio = [(1-[(Voc.VM_all.output_struct.intern(1:length(Voc.VM_all.output_struct.intern)).(RO2(k)))]./((Voc.VM_all.output_struct.intern(1).(RO2(k)))))*100];
HO2_ratio = [(1-[(Voc.VM_all.output_struct.intern(1:length(Voc.VM_all.output_struct.intern)).HO2)]./((Voc.VM_all.output_struct.intern(1).HO2)))*100];
OH_value = [Voc.VM_all.output_struct.intern(1:length(Voc.VM_all.output_struct.intern)).OH];
NOmr_log= log10(NOmr);
if k==1
    Comparison1 = table(NOmr.',NOmr_log.',RO2_ratio.',HO2_ratio.',OH_value.', 'VariableNames', ["NOmr","NOmr_log","RO2_ratio","HO2_ratio","OH_value"]);
elseif k==2
    Comparison2 = table(NOmr.',NOmr_log.',RO2_ratio.',HO2_ratio.',OH_value.', 'VariableNames', ["NOmr","NOmr_log","RO2_ratio","HO2_ratio","OH_value"]);
else
    Comparison3 = table(NOmr.',NOmr_log.',RO2_ratio.',HO2_ratio.',OH_value.', 'VariableNames', ["NOmr","NOmr_log","RO2_ratio","HO2_ratio","OH_value"]);
end

figure
plot (NOmr_log,RO2_ratio,'LineWidth',2,'Marker','^')
hold on; grid on;
plot (NOmr_log,HO2_ratio,'LineWidth',2,'Marker','x')
xlabel('logNO [mr]'); ylabel('Reduction Ratio');
legend('RO2','HO2','Location','Best')
title (VOC(k),["Reduction comparison Intern"])

    if save_figures == "Yes"
        filename = strcat("Reduction_comparison_RO2_HO2_intern");
        saveas(gca, fullfile(fname, VOC, filename), 'fig');
    end

%OH value plot
figure
plot (NOmr_log,OH_value,'LineWidth',2,'Marker','^')
hold on; grid on;
xlabel('logNO [mr]'); ylabel('Total OH mixing ratio');
legend('OH','Location','Best')
title (VOC(k),["OH progress"])

    if save_figures == "Yes"
        filename = strcat("OH_vs_added_NO_intern");
        saveas(gca, fullfile(fname, VOC, filename), 'fig');
    end
end