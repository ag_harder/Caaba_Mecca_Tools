function runcaaba_v2(InputNCFile, NcFileName, PathOutput, IniFileName, BatFileName, NmlFileName,PathStargate)
%function prepares exectuable script and starts caaba in the VM 
%   InputNCFile specifies the input data in a netcdf file to be used by
%       caaba, this file is moved into the input/multirun subdirectory
%   PathOutput ; not implemented; specifies where the reusltsfile should be
%       copied to
%   IniFileName specifies the INI file being used by xcaaba.py in the
%       virtual machine

    if (nargin<7)
        PathStargate="D:\VM\stargate";
    end
    
    %IPVM="192.168.17.129";
    IPVM="192.168.17.130";
    %IPVM="127.0.0.1";

    PathStargateTemp=fullfile(PathStargate,"temp");
    [status,msg]=mkdir(PathStargateTemp);
    copyfile(InputNCFile,PathStargateTemp);
    
    %create the .bat file
    ScriptnameBAT=fullfile(PathStargateTemp,BatFileName);
    fileBAT = fopen(ScriptnameBAT,"w");
    fprintf(fileBAT,'# -*- Shell-script -*-%c',newline);
    %fprintf(fileBAT,"%c",newline);
    fprintf(fileBAT,'# The shell variables defined here will be used by xmecca%c',newline)
    fprintf(fileBAT,'# When it is run in batch mode (i.e. not interactive).....%c',newline)
    %fprintf(fileBAT,"%c",newline);
    fprintf(fileBAT,'set apn = 0%c',newline);
    %fprintf(fileBAT,"%c",newline);
    fprintf(fileBAT,'set gaseqnfile = gas.eqn;%c',newline);
    fprintf(fileBAT,'set rplfile = CLD_intern;%c',newline);
    fprintf(fileBAT,'set wanted = "St && G"%c',newline);
    fprintf(fileBAT,'set setfixlist = "O2; N2;"%c',newline);
    fprintf(fileBAT,'set mcfct = n%c',newline);
    fprintf(fileBAT,'set diagtracfile =%c',newline);
    fprintf(fileBAT,'set rxnrates = y%c',newline);
    fprintf(fileBAT,'set enthalpy = n%c',newline);
    fprintf(fileBAT,'set tag = n%c',newline);
    fprintf(fileBAT,'set tagdbl = n%c',newline);
    fprintf(fileBAT,'set kppoption = k%c',newline);
    fprintf(fileBAT,'set integr = rosenbrock_posdef%c',newline);
    fprintf(fileBAT,'set decomp = n%c',newline);
    fprintf(fileBAT,'set latex = y%c',newline);
    fprintf(fileBAT,'set graphviz = n%c',newline);
    fprintf(fileBAT,'set deltmp = y%c',newline);
    fprintf(fileBAT,'set ignoremassbalance');

    %create the .nml file
    ScriptnameNML=fullfile(PathStargateTemp,NmlFileName);
    fileNML = fopen(ScriptnameNML,"w");
    fprintf(fileNML,"! -*- f90 -*-%c",newline);
    fprintf(fileNML,"&CAABA%c",newline);
    fprintf(fileNML,"USE_MECCA = T%c",newline);
    fprintf(fileNML,"USE_READJ = T%c",newline);
    fprintf(fileNML,"relhum = 0.00%c",newline);
    fprintf(fileNML,"temp = 300.15%c",newline);
    fprintf(fileNML,"press = 800%c",newline);
    fprintf(fileNML,"l_ignore_relhum = T%c",newline);
    fprintf(fileNML,"photrat_channel = 'readj'%c",newline);
    fprintf(fileNML,'%s',["init_spec = 'input/multirun/" NcFileName]);
    fprintf(fileNML,"'");
    fprintf(fileNML,"%c",newline);
    %fprintf(fileNML,"init_spec = 'input/multirun/CLD_carol_test.nc%c",newline);
    %fprintf(fileNML,"input_readj = 'input/multirun/CLD_carol_test.nc%c",newline);
    fprintf(fileNML,'%s',["input_readj = 'input/multirun/" NcFileName]);
    fprintf(fileNML,"'");
    fprintf(fileNML,"%c",newline);
    fprintf(fileNML,"%c",newline);
    fprintf(fileNML,"runtime_str = '0.060 seconds'%c",newline);
    fprintf(fileNML,"timesteplen_str = '0.0001 seconds'%c",newline);
    fprintf(fileNML,"%c",newline);
    fprintf(fileNML,"/%c",newline);
    
    %create a .ini file
    ScriptnameINI=fullfile(PathStargateTemp,IniFileName);
    fileINI = fopen(ScriptnameINI,"w");
    fprintf(fileINI,"[DEFAULT]%c",newline);
    fprintf(fileINI,"[xcaaba]%c",newline);
    fprintf(fileINI,'%s',['batfile = user_contributed/' BatFileName]);
    fprintf(fileINI,"%c",newline);
    fprintf(fileINI,"compile = c%c",newline);
    fprintf(fileINI,'%s',['nmlfile =' NmlFileName]);
    fprintf(fileINI,"%c",newline);
    fprintf(fileINI,"l_montecarlo = False%c",newline);
    fprintf(fileINI,"runcaaba = m%c",newline);
    fprintf(fileINI,'%s',['multirun_ncfile =' NcFileName]);
    fprintf(fileINI,"%c",newline);
    fprintf(fileINI,"outputdir = %c",newline);
    fprintf(fileINI,"l_caabaplot = True%c",newline);
    fprintf(fileINI,"[caabaplot]%c",newline);
    fprintf(fileINI,"modelruns = [['.', 'latest run'],]%c",newline);
    fprintf(fileINI,"plotspecies = O3, NO, NO2, NOx, NO2D%c",newline);
    fprintf(fileINI,"plotjvals = %c",newline);
    fprintf(fileINI,"plotrxns = %c",newline);
    fprintf(fileINI,"plotbudget = O3, NO, NO2, NOx, NO2D%c",newline);
   
    
    % create list of commands executed in the VM
    Scriptname=fullfile(PathStargate,"StartCaaba.sh");
    fileID = fopen(Scriptname,"w");
    %test = join(["mv -f /mnt/hgfs/stargate/temp/" NcFileName "/home/caaba/caaba_4.4.1/input/multirun%c",newline])
    fprintf(fileID,'%s',["cp -f /mnt/hgfs/stargate/temp/" NcFileName " /home/caaba/caaba_4.4.1/input/multirun"])
    fprintf(fileID,"%c",newline);
    fprintf(fileID,'%s',["cp -f /mnt/hgfs/stargate/temp/" IniFileName " /home/caaba/caaba_4.4.1/ini"])
    fprintf(fileID,"%c",newline);
    fprintf(fileID,'%s',["cp -f /mnt/hgfs/stargate/temp/" BatFileName " /home/caaba/caaba_4.4.1/mecca/batch/user_contributed"])
    fprintf(fileID,"%c",newline);
    fprintf(fileID,'%s',["cp -f /mnt/hgfs/stargate/temp/" NmlFileName " /home/caaba/caaba_4.4.1/nml"])
    fprintf(fileID,"%c",newline);
    fprintf(fileID,"cd /home/caaba/caaba_4.4.1/%c",newline);
    fprintf(fileID,"%c",newline);
    fprintf(fileID,"./xcaaba.py -i %s %c",IniFileName,newline); % CLD_internal.ini
    fprintf(fileID,"%c",newline);
    fprintf(fileID,"rsync -az /home/caaba/caaba_4.4.1/output/ /home/caaba/Desktop/stargate/output/%c",newline);
    fprintf(fileID,"%c",newline);
    % execute script in VM
    command=join(["putty " IPVM "-ssh -l caaba -pw CaMe4ViMa -m" Scriptname])
    system(command)

    %fprintf(fileID,"%c",newline);
    %fprintf(fileID,test);
    %fprintf(fileID,"mv -f /mnt/hgfs/stargate/temp/",IniFileName," /home/caaba/caaba_4.4.1/ini%c",newline);
    %fprintf(fileID,"mv -f /mnt/hgfs/stargate/temp/",BatFileName," /home/caaba/caaba_4.4.1/mecca/batch/user_contributed%c",newline);
    %fprintf(fileID,"mv -f /mnt/hgfs/stargate/temp/",NmlFileName," /home/caaba/caaba_4.4.1/nml%c",newline);
    %fprintf(fileID,"cd /home/caaba/caaba_4.4.1/%c",newline);
    %fprintf(fileID,"./xcaaba.py -i %s %c",newline,IniFileName); % CLD_internal.ini
    %fprintf(fileID,"rsync -az /home/caaba/caaba_4.4.1/output/ /home/caaba/Desktop/stargate/output/%c",newline);
    % execute script in VM
    %command=join(["putty " IPVM "-ssh -l caaba -pw CaMe4ViMa -m" Scriptname])
    %system(command)
    
    % copy files from stargate to specified output
    % ....


end