clearvars

stargate_path = 'C:\Users\F.Kunkler\Desktop\VM\stargate';
% stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';

VOC =["C5H8"];
load(fullfile(stargate_path,'output_matlab',strcat('output_all_',(VOC),'.mat')));
budget_path = 'C:\Users\F.Kunkler\Desktop\VM\Budget';
% budget_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\Budget';
budget_for = ["OH"];

% RO2 values for C5H8
RO2=["ISOPBO2","LISOPACO2","ISOPDO2","LDISOPACO2","LISOPEFO2"];
RO2_values = table;

budget_for = RO2;

for k=1:length(RO2)
    RO2_values.(RO2(k))=[VM_all.output_struct.after.(RO2(k))].';
end
RO2_values.Summe_RO2 = sum(table2array(RO2_values),2);
RO2_values.OH = [VM_all.output_struct.after.OH].';
RO2_values=table2struct(RO2_values);

%%
budgets = struct;
under_after = ["under","after"];
%% Choose Run and what u want 


for k=1:length(budget_for) 
    datfile_path(k)=strcat(budget_path,'\budgetUnKnwn',budget_for(k),'.dat');
end

VM_all.sum_RR_output_struct.after(1).RRG4153=0;
VM_all.sum_RR_output_struct.after(1).RRG4155 =0;
%% get Values for Budgets
for m=1:length (under_after)
for l=1:length(budget_for) 
for k=1:length(VM_all.sum_RR_output_struct.(under_after(m)))
    budgets.(under_after(m)).(budget_for(l))(k)= budgetcalc_C5H8(datfile_path(l),VM_all.sum_RR_output_struct.(under_after(m))(k)); 
end
end
end


%% 
%in ProdRxn Reaktionsgleichungen der Produktion
VOCmr = [VM_all.output_struct.(under_after(1)).(VOC)].'; %[] holt die VOC mr aus dem zugehörigen raus

%% in Loss all loss data per timestep same for prod
prod = struct; loss = struct;

manual_end_prod_OH =107;  %if there are different numbers of reactions
manual_end_loss_OH = 117;
manual_end_loss_VOC = 1;

for l=1:length(budget_for)
    for m=1:length (under_after)
    if budget_for(l)=="OH"
            for k=1:length(VOCmr)
                if k == 1
                    prod0(:,(k)) = budgets.(under_after(m)).(budget_for(l))(k).Prod;
                    loss0(:,(k)) = budgets.(under_after(m)).(budget_for(l))(k).Loss;
                else
                    prod1 = flip(budgets.(under_after(m)).(budget_for(l))(k).Prod);
                    prod_cut(:,(k-1)) = flip(prod1(1:manual_end_prod_OH));
                    loss1 = flip(budgets.(under_after(m)).(budget_for(l))(k).Loss);
                    loss_cut(:,(k-1)) = flip(loss1(1:manual_end_loss_OH));      
                end
            end
                    prod0 = prod0.'; prod0(2,:)=repmat(0,[1,length(prod0)]);
                    prod_cut = prod_cut.';
                    loss0 = loss0.'; loss0(2,:)=repmat(0,[1,length(loss0)]);
                    loss_cut = loss_cut.';
                    prod.(under_after(m)).OHzeroVOC = prod0;prod.(under_after(m)).(budget_for(l)) = prod_cut;
                    loss.(under_after(m)).OHzeroVOC = loss0;loss.(under_after(m)).(budget_for(l)) = loss_cut;
                    prod0=[]; loss0=[]; prod_cut=[]; loss_cut=[]; loss1 =[];

    else  
            for k=2:length(VOCmr)
            prod_cut(:,(k-1)) = budgets.(under_after(m)).(budget_for(l))(k).Prod;
            loss_cut(:,(k-1)) = budgets.(under_after(m)).(budget_for(l))(k).Loss;   
            end
            prod_cut = prod_cut.';prod.(under_after(m)).(budget_for(l)) = prod_cut;
            loss_cut = loss_cut.';loss.(under_after(m)).(budget_for(l)) = loss_cut;
            prod_cut=[]; loss1=[]; loss_cut=[];

        
         end
    end
    end


clear loss0 loss1 loss2 loss3 prod0 prod1 prod2 prod3 loss_cut
%% Plot of Loss and Prod


%if u want all 4 in 1

% figure;
% for l=1:length(budget_for)
% if l==1
%     subplot(4,1,l);
% elseif l==2
%     subplot(4,1,3)
% end
% title(strcat("Production of " , budget_for(l)))
% hold on; grid on;
% bar(VOCmr(2:end), prod.(budget_for(l)) ,'stacked')
% legend(budgets.(budget_for(l))(4).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
% legend('toggle')
% xlabel('time [s]');ylabel(strcat(budget_for(l), " [mr]"));
% subplot(4,1,2*l);
% hold on; grid on;
% title(strcat("Loss of " , budget_for(l)))
% bar(VOCmr(2:end), loss.(budget_for(l)) ,'stacked')
% legend(budgets.(budget_for(l))(4).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
% legend('toggle')
% xlabel('time [s]');ylabel(strcat(budget_for(l), " [mr]"));
% end
% hold off

for l=1:length(budget_for)
  for m=1:length (under_after)
      %to reduce the number of plottet and showed reactions to max 4
     [a,b]=size(prod.(under_after(m)).(budget_for(l)));
     [c,d]= size(loss.(under_after(m)).(budget_for(l)));
     %for prod
     if b>=4
         if under_after(m)=="after"
            prod_plotend=(prod.(under_after(m)).(budget_for(l))(:,end-4:end));
            prod_plot_others=sum(prod.(under_after(m)).(budget_for(l))(:,1:end-5),2);
            prod_plot =[prod_plot_others,prod_plotend];
         %legend
            legend_prod = budgets.(under_after(m)).(budget_for(l))(4).ProdRxn(end-5:end);
            legend_prod(1)={'Others'};
         else
             prod_plotend=(prod.(under_after(m)).(budget_for(l))(:,end-2:end));
             prod_plot_others=sum(prod.(under_after(m)).(budget_for(l))(:,1:end-3),2);
            prod_plot =[prod_plot_others,prod_plotend];
            %legend
            legend_prod = budgets.(under_after(m)).(budget_for(l))(4).ProdRxn(end-3:end);
             legend_prod(1)={'Others'};
         end
     else
         prod_plot=prod.(under_after(m)).(budget_for(l));
         legend_prod = budgets.(under_after(m)).(budget_for(l))(4).ProdRxn;
     end
     
     %for loss
     if d>=4
         loss_plotend=(loss.(under_after(m)).(budget_for(l))(:,end-2:end));
         loss_plot_others=sum(loss.(under_after(m)).(budget_for(l))(:,1:end-3),2);
         loss_plot =[loss_plot_others,loss_plotend];
         %legend
         legend_loss = budgets.(under_after(m)).(budget_for(l))(4).LossRxn(end-3:end);
         legend_loss(1)={'Others'};
     else
         loss_plot = loss.(under_after(m)).(budget_for(l));
         legend_loss = budgets.(under_after(m)).(budget_for(l))(4).LossRxn;
     end
figure;
subplot(2,1,1);
title(strcat("Production of " , budget_for(l) ," ",(under_after(m))," lamp"))
hold on; grid on;
bar(VOCmr(2:end), prod_plot,'stacked')
legend(legend_prod,'Location','northeast','FontSize',9)%(4) without a special reason (in hope at every time the same reactions)
xlabel(strcat((VOC)," [mr]"));ylabel(strcat(budget_for(l), " [mr/s]"));



subplot(2,1,2);
hold on; grid on;
title(strcat("Loss of " , budget_for(l) ," ",(under_after(m))," lamp"))
bar(VOCmr(2:end), loss_plot ,'stacked')
legend(legend_loss,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
xlabel(strcat((VOC)," [mr]"));ylabel(strcat(budget_for(l), " [mr/s]"));
  end
end
hold off
%% Plot Zero-Value OH without VOC
 for m=1:length (under_after)
figure;
subplot(2,1,1);
title(strcat("Production of OH without VOC"," ",(under_after(m))," lamp"));
hold on; grid on;
bar([1,2],prod.(under_after(m)).OHzeroVOC,'stacked')
legend(budgets.(under_after(m)).OH(1).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel(strcat((VOC)," mr = 0"));ylabel(" OH ");


subplot(2,1,2);
hold on; grid on;
title(strcat("Loss of OH without VOC", " ",(under_after(m))," lamp") )
bar([1,2],loss.(under_after(m)).OHzeroVOC,'stacked')
legend(budgets.(under_after(m)).OH(1).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
legend('toggle')
xlabel(strcat((VOC)," mr = 0"));ylabel(" OH ");
hold off

 end
