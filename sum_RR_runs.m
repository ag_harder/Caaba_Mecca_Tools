
function [sum_RR_runs] = sum_RR_runs(stargate_path,filename_under,filename_after)

%% klappt alles nur wenn wir ein VOC betrachten
output_struct = struct('under',[],'after',[]);
u = load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_under,'.mat')));
output_struct(1).under =u.output_struct;
a = load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_after,'.mat')));
output_struct.after =a.output_struct;

runs = struct('under',[],'after',[]);
u = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_under,'.mat')));
runs(1).under =u.runs;
a = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_after,'.mat')));
runs.after =a.runs;

clear a u

%% Gets the sum in 1 table
%for f�r after/under und for f�r jeden run
after_under = ["under","after"];
sum_RR_runs = struct;
for k=1:length(after_under)
    for l=1:length(fields(runs.(after_under(k))))
        if l<10
            run = strcat('run000',num2str(l));
        elseif l>=10
            run = strcat('run00',num2str(l));
        elseif l>=100
            run = strcat('run0',num2str(l));
        end
        fieldnames_run = fieldnames(runs.(after_under(k)).(run));
        fieldnames_RR = fieldnames_run(contains(fieldnames(runs.(after_under(k)).(run)),'RR'));
        T2=setdiff(fieldnames_run,fieldnames_RR);
        
        if k==1
        sum_run = sum(table2array(struct2table(runs.(after_under(k)).(run))),1);
        elseif k==2
        sum_run = sum(table2array(struct2table(runs.(after_under(k)).(run)),1));
        end
        
        sum_run = array2table(sum_run);
        sum_run.Properties.VariableNames= fieldnames_run;
        sum_run = removevars(sum_run,T2); %removes all fields without 'RR'
        
%         sum_run=setdiff(fieldnames_run,T2);
% T2 = removevars(T1,{'Loss','Customers'});
        %turns into struct
        sum_RR_runs.(after_under(k)).(run)=table2struct(sum_run);
        end
end