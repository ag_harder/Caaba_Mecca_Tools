clearvars;
stargate_path='C:\Users\F.Kunkler\Desktop\VM\stargate\';

load('C:\Users\F.Kunkler\Desktop\VM\stargate\output_matlab\output_50lmin_CH4_00088.mat');
load('C:\Users\F.Kunkler\Desktop\VM\stargate\output_matlab\output_runs_50lmin_CH4_00088.mat');

% �ndern dass: OH konz aus letztem run als startkonzentration festgelegt
% wird ebenso VOC und RO2 konzentration 
%!!!! j_H2O soll bzw muss 0 sein l�fut danach noch 15 sek

%constants
p0=101325 ; t0=298.15; kB=1.380649E-23; % Pa, K, J/K
lozahl=(p0/(t0*kB)/1000000); %1/cm^3
VOCmr=[];VOCmr2=[];

%%Choose VOC
var_name_VOC=["CH4"];
var_name_RO2=["CH3O2"];
H2Opercent = 0.01;
H2Oin = (lozahl*H2Opercent)/lozahl;


%calculate concentration of VOC to react with OH -1=log([OH]t/[OH]0)
%for a Halflifetime of 10 ms
temp=298.15; %K
time= 0.01; %s


%for schleife
for k=1:length(var_name_VOC)
  if strcmp(var_name_VOC(k),'CH4') %Methan 
    kVOC= 1.85E-20*exp(2.82*log(temp)-987./temp);
  elseif strcmp(var_name_VOC(k),'C2H6') %Ethan
    kVOC= 1.49E-17*temp*temp*exp(-499./temp);
  elseif strcmp(var_name_VOC(k),'C3H8') %Propan
    kVOC= 4.50E-18*temp*temp*exp(253./temp); %Isoprop
    kVOC2= 2*4.49E-18*temp*temp*exp(-320./temp); %n-prop
  elseif strcmp(var_name_VOC(k),'C2H4'); %Ethen
    kVOC= third_order_reaction_rate(temp,lozahl,8.6E-29,3.1,9.E-12,0.85,0.48);
  elseif strcmp(var_name_VOC(k),'C3H6') %Propen
    kVOC= third_order_reaction_rate(temp,lozahl,8.6E-27,3.5,3.E-11,1.,0.5);
  elseif strcmp(var_name_VOC(k),'BENZENE') %Benzol
    kVOC= 2.3E-12*exp(-190/temp);
  elseif strcmp(var_name_VOC(k),'TOLUENE') %Toluol
    kVOC= 1.8E-12*exp(340/temp);
  elseif strcmp(var_name_VOC(k),'C5H8') %Isopren
    kVOC=2.7E-11*exp(390./temp);
  elseif strcmp(var_name_VOC(k),'APINENE') %a-Pinen
    kVOC=1.2E-11*exp(440./temp);    
  elseif strcmp(var_name_VOC(k),'limonene') %Limonen
    kVOC=1; 
  end
  
concVOC=(-1/(-kVOC*time));
VOCc=[0,0.5*concVOC,concVOC,2*concVOC];
VOCa=((VOCc)/lozahl);
VOCmr=[VOCmr;VOCa]; VOCmr2=[VOCmr2,VOCa];
end



%% input test values
temp=298.15; %K
press=101325; %Pa
CO2 = 0;
j_H2O = 0; %
% VOC = VOCmr;
% RO2 = [output_struct(1:4).CH3O2] ;
% OH = [output_struct(1:4).OH];

VOCmr = [0.0087779742,0.0087779742];
VOCmr2 = VOCmr;
RO2 = [0,6.59989493612123e-11];
OH = [0,5.2410715e-12];

length_array=length(VOCmr2);

model_input.nml_press=repmat(press,[1,length_array]); %Pa standard
model_input.nml_temp=repmat(temp,[1,length_array]); %K  standard
model_input.H2O=repmat(H2Oin,[1,length_array]); % 1% H2O 
% model_input.j_H2O=repmat(j_H2O,[1,length_array]); % J x sigma
model_input.j_H2O = [7.2E-7,j_H2O];
model_input.Tgps=repmat(1:length_array,[1,1]);
model_input.CO2=repmat(CO2,[1,length_array]);
% model_input.(var_name_VOC)=VOC;
model_input.(var_name_VOC)=VOCmr;
model_input.(var_name_RO2)=RO2;
model_input.OH = OH;

% runtime_str= "'0.001 seconds'";
% timesteplen_str= "'0.0001 seconds'";
% model_input.runtime_str=str2double(repmat(runtime_str,[1,length_array]));
% model_input.timesteplen_str=str2double(repmat(timesteplen_str,[1,length_array]));

%% Create netCDF file from MatLab dataset

if j_H2O > 0
    fname    = 'RO2_Felix_Multi_under_lamp.nc';
elseif  j_H2O ==0
    fname    = 'RO2_Felix_Multi_after_lamp.nc';
end

varnames=fields(model_input);

if exist([stargate_path,fname]) %#ok<EXIST>
    fclose('all');
    clear mex %#ok<CLMEX>
    delete([stargate_path,fname]);
end

ncid     = netcdf.create([stargate_path,fname],'NC_NOCLOBBER'); % Open netCDF file.
len_data = [size(model_input.Tgps,2),size(varnames,1)];   % Define the dimensions of the variable
%dim      = len_data(1)+1;
%dimid    = netcdf.defDim(ncid,'T_AX',len_data);
dimid    = netcdf.defDim(ncid,'time',netcdf.getConstant('NC_UNLIMITED'));

for i = 1 : len_data(2)                          % Define a new variable in the file.
    varID   = netcdf.defVar(ncid,varnames{i},'double',dimid);
    netcdf.endDef(ncid);                    % Leave define mode and enter data mode to write data.
    netcdf.putVar(ncid,varID,0,len_data(1), eval(['model_input.',varnames{i}]));      % Write data to variable.
    netcdf.reDef(ncid);                     % Leave data mode and re-enter define mode
    % netcdf.putAtt(ncid,varID,'units','mixing ratio'); % define attribute for missing value
end
netcdf.close(ncid);

