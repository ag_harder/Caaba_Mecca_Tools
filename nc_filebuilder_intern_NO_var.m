clearvars;
stargate_path='C:\Users\F.Kunkler\Desktop\2020_RO2_HO2\stargate\';
stargate_path='C:\Users\f.kunkler\Documents\Seafile\2020_RO2_HO2\stargate\';
%VOC + DATUM im file/savename anpassen!
VOC = "zero";

switch VOC
    
    case "CH4"
       filename = convertStringsToChars(strcat(VOC,'_50lmin_03-Dec-2020_after_lamp_with_CO'));
       savename = convertStringsToChars(strcat(VOC,'_50lmin_03-Dec-2020_intern_NOvar_with_CO'));
    case "C2H4"
       filename = convertStringsToChars(strcat(VOC,'_50lmin_04-Nov-2020_after_lamp'));
       savename = convertStringsToChars(strcat(VOC,'_50lmin_04-Nov-2020_intern_NOvar'));
    case "BENZENE"
       filename = convertStringsToChars(strcat(VOC,'_50lmin_09-Nov-2020_after_lamp'));
       savename = convertStringsToChars(strcat(VOC,'_50lmin_09-Nov-2020_intern_NOvar'));
    case "C5H8"
       filename = convertStringsToChars(strcat(VOC,'_50lmin_09-Nov-2020_after_lamp'));
       savename = convertStringsToChars(strcat(VOC,'_50lmin_09-Nov-2020_intern_NOvar'));
    case "C3H6"
       filename = convertStringsToChars(strcat(VOC,'_50lmin_12-Nov-2020_after_lamp'));
       savename = convertStringsToChars(strcat(VOC,'_50lmin_12-Nov-2020_intern_NOvar'));
    case "CO"
       filename = convertStringsToChars(strcat(VOC,'_50lmin_20-Nov-2020_after_lamp'));
       savename = convertStringsToChars(strcat(VOC,'_50lmin_20-Nov-2020_intern_NOvar'));
    case "C3H8"
       filename = convertStringsToChars(strcat(VOC,'_50lmin_27-Nov-2020_after_lamp'));
       savename = convertStringsToChars(strcat(VOC,'_50lmin_27-Nov-2020_intern_NOvar'));  
    case "APINENE"
       filename = convertStringsToChars(strcat(VOC,'_50lmin_08-Dec-2020_after_lamp'));
       savename = convertStringsToChars(strcat(VOC,'_50lmin_08-Dec-2020_intern_NOvar'));
    case "H2O"
       filename = convertStringsToChars(strcat(VOC,'_50lmin_30-Apr-2021_after_lamp_without_CH4'));
       savename = convertStringsToChars(strcat(VOC,'_50lmin_30-Apr-2021_intern_NOvar'));
    otherwise
       filename = convertStringsToChars(strcat('2021_Mixed_VOC_30-Aug-2021_after_lamp_new_J'));
          
end





%loads Output_structs of the run After the Lamp
load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename,'.mat')));
load(fullfile(stargate_path,'output_matlab',strcat('output_',filename,'.mat')));


%constants
p0=101325 ; t0=298.15; kB=1.380649E-23; % Pa, K, J/K
lozahl=(p0/(t0*kB)/1000000); %1/cm^3
H2Opercent = 0.01;
H2Oin = (lozahl*H2Opercent)/lozahl;



%% Goes into the Output_struct to read which fields are in and delete the Reactionrates


fieldnames_output = fieldnames(output_struct);
fieldnames_RR = fieldnames_output(contains(fieldnames(output_struct),'RR'));
fieldnames_remove = cat(1,fieldnames_RR,'Tgps','nml_press','nml_temp');
output_struct_slim = removevars(struct2table(output_struct),fieldnames_remove);
output_struct_slim = table2struct(output_struct_slim);

%Input off the fields into struct

fieldnames_input = fieldnames(output_struct_slim);
model_input = struct;


%% Standard
temp=298.15; %K
press=373; %Pa
j_H2O = 0; %
CO2 = 0;
CO = 0;
% HO2 = 0;
%     NOmr = 1.54E-7;

NO =[0;1.57000000000000e-05;2.52000000000000e-05;3.15000000000000e-05;4.72000000000000e-05;6.30000000000000e-05;9.45000000000000e-05;0.000157000000000000;0.000252000000000000;0.000472000000000000;0.000630000000000000;0.000944000000000000;0.00126000000000000;0.00142000000000000;0.00157000000000000;0.00173000000000000;0.00189000000000000;0.00204000000000000;0.00220000000000000;0.00236000000000000;0.00251000000000000];
runnum = 7;
savename = convertStringsToChars(strcat('2021_Mixed_VOC_30-Aug-2021_',num2str(runnum)));

length_array = length(NO);
Filter = 1E-20;
for k=1: length(fieldnames_input)
    if (output_struct(runnum).(string(fieldnames_input(k)))<= Filter) == true
        %model_input.(string(fieldnames_input(k))) = zeros(1,length(NO)); 
    else
        model_input.(string(fieldnames_input(k))) = repmat([output_struct(runnum).(string(fieldnames_input(k)))],[1,length(NO)]); 
    end
end


if runnum == 8
   model_input.SC4H9O2 = 0.873*model_input.LC4H9O2; 
   model_input.NC4H9O2 = (1-0.873)*model_input.LC4H9O2; 
   model_input.LC4H9O2 = zeros(1,length(NO));
end




%final input !!!if CH4 is a field in output_struct it ignores the CH4 = 0!!
if isfield ( output_struct,'CH4')
    model_input.nml_press=repmat(press,[1,length_array]); %Pa standard
    model_input.nml_temp=repmat(temp,[1,length_array]); %K  standard
    %model_input.H2O=repmat(H2Oin,[1,length_array]); % 1% H2O
    model_input.j_H2O=repmat(j_H2O,[1,length_array]); % J x sigma
    model_input.Tgps=repmat(1:length_array,[1,1]);
    model_input.NO=NO;
%     model_input.HO2=repmat(HO2,[1,length_array]); %
else
    model_input.nml_press=repmat(press,[1,length_array]); %Pa standard
    model_input.nml_temp=repmat(temp,[1,length_array]); %K  standard
    %model_input.H2O=repmat(H2Oin,[1,length_array]); % 1% H2O 
    model_input.j_H2O=repmat(j_H2O,[1,length_array]); % J x sigma
    model_input.Tgps=repmat(1:length_array,[1,1]);
    model_input.NO=NO;
    model_input.CH4 = zeros(1,length_array);
%     model_input.HO2=repmat(HO2,[1,length_array]); %
end


% model_input.CH4 = repmat(CH4,[1,length_array]);
% model_input.CO = repmat(CO,[1,length_array]);

%% Create netCDF file from MatLab dataset

fname    = strcat(savename,'_modified.nc');


varnames=fields(model_input);

if exist([stargate_path,fname]) %#ok<EXIST>
    fclose('all');
    clear mex %#ok<CLMEX>
    delete([stargate_path,fname]);
end

ncid     = netcdf.create([stargate_path,fname],'NC_NOCLOBBER'); % Open netCDF file.
len_data = [size(model_input.Tgps,2),size(varnames,1)];   % Define the dimensions of the variable
%dim      = len_data(1)+1;
%dimid    = netcdf.defDim(ncid,'T_AX',len_data);
dimid    = netcdf.defDim(ncid,'time',netcdf.getConstant('NC_UNLIMITED'));

for i = 1 : len_data(2)                          % Define a new variable in the file.
    varID   = netcdf.defVar(ncid,varnames{i},'double',dimid);
    netcdf.endDef(ncid);                    % Leave define mode and enter data mode to write data.
    netcdf.putVar(ncid,varID,0,len_data(1), eval(['model_input.',varnames{i}]));      % Write data to variable.
    netcdf.reDef(ncid);                     % Leave data mode and re-enter define mode
    % netcdf.putAtt(ncid,varID,'units','mixing ratio'); % define attribute for missing value
end
netcdf.close(ncid);

