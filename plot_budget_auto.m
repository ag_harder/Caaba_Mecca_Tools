clearvars
Config_at_userpath;

% stargate_path = 'C:\Users\Felix\Desktop\Mastersachen\VM\stargate';

VOC =["C3H8"];
save_figures = ["NO"];
under_after = ["intern"]; %"under","after",

switch VOC
    case "Dummy"
        RO2 = [];
        side_products = [];  
        intern_RO2_product = [];   % 
        intern_RO2_side =[]; % Preproducts..
    case "CH4"
        RRG_VOC = ["RRG4101","RRG3200","RRG3201"]; %CH4 + OH = CH3 + H2O , HONO ,HO2+NO
        RO2 = ["CH3O2","CH3O","HCHO"];
        side_products = ["CH3OOH","HCHO"];
        intern_RO2_product = ["HCHO"];   % Endproduct of CH3O2
        intern_RO2_side =["CH3ONO","CH3O"]; % sideproducts of CH3O2 + NO and further
    case "C2H4"
        RRG_VOC = ["RRG42002","RRG3200","RRG3201"];
        RO2 = ["HOCH2CH2O2"];
        side_products = ["HOCH2CH2O","HOCH2CHO","ETHGLY","HYETHO2H"];
        intern_RO2_product = ["HOCH2CHO","HCHO"];   % HCHO is the main Product  HOCH2CH2O2 + NO   = .25 HO2 + .5 HCHO + .75 HOCH2CH2O + NO2  !4 Größenordnungen höher als andere Reaktionen von RO2!
        intern_RO2_side =["HOCH2CH2O","ETHOHNO3"]; % sideproducts 
    case "BENZENE"
        RRG_VOC = ["RRG46441","RRG3200","RRG3201"];
        RO2 = ["BZBIPERO2"]; % Reagiert mit NO BZBIPERO2  + NO     = NO2 + GLYOX + HO2 + .5 BZFUONE + .5 BZFUONE  2 Größenordnungen schneller als andere reaktionen von RO2
        side_products = ["PHENOL","BZEPOXMUC","CATECHOL","BZEMUCO2","BZEMUCCO3"];
        intern_RO2_product = ["GLYOX","BZFUONE"];   % 
        intern_RO2_side =["BZBIPERNO3"]; % 
    case "C5H8"
        RO2 = ["LISOPACO2","ISOPBO2","LDISOPACO2","ISOPDO2","LISOPEFO2"];
        side_products = ["LISOPAB","LISOPCD","C1ODC2O2C4OOH","C1OOHC3O2C4OD","LZCODC23DBCOOH","ISOPAOH","LISOPACOOH"];
    case "C3H6"
        RRG_VOC = ["RRG43002","RRG3200","RRG3201"];
        RO2 = ["HYPROPO2"];
        side_products = ["HYPROPO2H","CH3CHO","ACETOL"];  
        intern_RO2_product = ["CH3CHO","HCHO"];   % 
        intern_RO2_side =["PROPOLNO3","HYPROPO2H","ACETOL"]; % 
    case "C3H8"
        RO2 = ["IC3H7O2","NC3H7O2"];
        side_products = ["NC3H7OOH","IC3H7OOH"];  
        intern_RO2_product = ["C2H5CHO","CH3COCH3","HCHO"];   % 
        intern_RO2_side =["NC3H7NO3","IC3H7NO3","CH3COCH2O2","CH3CO","IPROPOL","HYPERACET","NOA","C2H5CO3"]; % Preproducts..
    case "APINENE"
        RO2 = ["LAPINABO2","ROO6R1O2","OHMENTHEN6ONEO2","PINALO2"];
        side_products = ["MENTHEN6ONE","LAPINABOOH","PINAL","PINALOOH"];  
        intern_RO2_product = ["PINAL","C106O2","OH2MENTHEN6ONE"];   % Pinal product of LAPINABO2+NO, C106O2 product of PINAL+NO
        intern_RO2_side =["LAPINABNO3","PINALNO3"]; % Preproducts.. 
    case "NC4H10"
        
    case "Zero"
        RO2=[];
end

load(fullfile(stargate_path,'output_matlab',strcat('output_all_BUT1ENE_modified.mat')));
% load(fullfile(stargate_path,'output_matlab',strcat('output_all_',(VOC),'_with_CO.mat')));
RO2 = ["HO2x","OHx","OH","HO2"];%

% Additional_Budgets =["PHENOL","BZEPOXMUC"];

for v=1:length(VOC)
    
    % Savepath and create the save folders
    if save_figures == "Yes" 
    fname = 'C:\Users\F.Kunkler\Desktop\VM\Figures';
    VOC_folder = fullfile(fname,VOC);
    Budget_folder = fullfile (VOC_folder,"Budget");
    
    
        if ~exist(VOC_folder, 'dir')  %creates the save folders
            mkdir(VOC_folder)
        end
        if ~exist(Budget_folder, 'dir')
            mkdir(Budget_folder)
        end
    end
    
    
budget_for=[];

for w=1:length(RO2)
    if isfield(VM_all.output_struct.(under_after(end)),(RO2(w)))==true
       budget_for= [budget_for,RO2(w)];
    end
end

% budget_for=[budget_for,Additional_Budgets]
%%
budgets = struct;

%% Choose Run and what u want 

for k=1:length(budget_for) 
    datfile_path(k)=strcat(budget_path,'budgetUnKnwn',budget_for(k),'.dat');
end
%% get Values for Budgets
for m=1:length (under_after)
for l=1:length(budget_for) 
for k=1:length(VM_all.sum_RR_output_struct.(under_after(m)))
    budgets.(under_after(m)).(budget_for(l))(k)= budgetcalc(datfile_path(l),VM_all.sum_RR_output_struct.(under_after(m))(k)); 
end
end
end


%% 
%in ProdRxn Reaktionsgleichungen der Produktion
if under_after(m) ~= "intern"
    
    VOCmr = [VM_all.output_struct.(under_after(1)).(VOC(v))].'; %[] holt die VOC mr aus dem zugehörigen raus
else
end

if any(contains(under_after,["intern"]))== true
    NOmr = [VM_all.output_struct.intern.NO];
end




%% NEW
gesamt=struct;

for j=1:length(under_after)
    for i=1:length(budget_for)
           gesamt.(under_after(j)).(budget_for(i)).ProdRxnNum=budgets.(under_after(j)).(budget_for(i)).ProdRxnNum;
           gesamt.(under_after(j)).(budget_for(i)).LossRxnNum=budgets.(under_after(j)).(budget_for(i)).LossRxnNum;
           gesamt.(under_after(j)).(budget_for(i)).ProdRxn=budgets.(under_after(j)).(budget_for(i)).ProdRxn;
           gesamt.(under_after(j)).(budget_for(i)).LossRxn=budgets.(under_after(j)).(budget_for(i)).LossRxn;
           for k=1:length(budgets.(under_after(j)).(budget_for(i)))
                gesamt.(under_after(j)).(budget_for(i)).ProdRxnNum=[gesamt.(under_after(j)).(budget_for(i)).ProdRxnNum;budgets.(under_after(j)).(budget_for(i))(k).ProdRxnNum];
                gesamt.(under_after(j)).(budget_for(i)).LossRxnNum=[gesamt.(under_after(j)).(budget_for(i)).LossRxnNum;budgets.(under_after(j)).(budget_for(i))(k).LossRxnNum];
                gesamt.(under_after(j)).(budget_for(i)).ProdRxn=[gesamt.(under_after(j)).(budget_for(i)).ProdRxn;budgets.(under_after(j)).(budget_for(i))(k).ProdRxn];
                gesamt.(under_after(j)).(budget_for(i)).LossRxn=[gesamt.(under_after(j)).(budget_for(i)).LossRxn;budgets.(under_after(j)).(budget_for(i))(k).LossRxn];
           end
           gesamt.(under_after(j)).(budget_for(i)).ProdRxnNum=unique(gesamt.(under_after(j)).(budget_for(i)).ProdRxnNum,'stable');
           gesamt.(under_after(j)).(budget_for(i)).LossRxnNum=unique(gesamt.(under_after(j)).(budget_for(i)).LossRxnNum,'stable');
           gesamt.(under_after(j)).(budget_for(i)).ProdRxn=unique(gesamt.(under_after(j)).(budget_for(i)).ProdRxn,'stable');
           gesamt.(under_after(j)).(budget_for(i)).LossRxn=unique(gesamt.(under_after(j)).(budget_for(i)).LossRxn,'stable');
           for k=1:length(budgets.(under_after(j)).(budget_for(i)))
               %For gesamt.xxx.xxx.Prod
               for m=1:length(gesamt.(under_after(j)).(budget_for(i)).ProdRxnNum)
                   if ~isempty(find((strcmp((unique(budgets.(under_after(j)).(budget_for(i))(k).ProdRxnNum,'stable')),gesamt.(under_after(j)).(budget_for(i)).ProdRxnNum(m)))))==true
                       gesamt.(under_after(j)).(budget_for(i)).Prod(m,k)=budgets.(under_after(j)).(budget_for(i))(k).Prod((strcmp((unique(budgets.(under_after(j)).(budget_for(i))(k).ProdRxnNum,'stable')),gesamt.(under_after(j)).(budget_for(i)).ProdRxnNum(m))));
                   else 
                       gesamt.(under_after(j)).(budget_for(i)).Prod(m,k)= 0;
                   end
               end
               %For gesamt.xxx.xxx.Loss
               for m=1:length(gesamt.(under_after(j)).(budget_for(i)).LossRxnNum)
                   if ~isempty(find((strcmp((unique(budgets.(under_after(j)).(budget_for(i))(k).LossRxnNum,'stable')),gesamt.(under_after(j)).(budget_for(i)).LossRxnNum(m)))))==true
                       gesamt.(under_after(j)).(budget_for(i)).Loss(m,k)=budgets.(under_after(j)).(budget_for(i))(k).Loss((strcmp((unique(budgets.(under_after(j)).(budget_for(i))(k).LossRxnNum,'stable')),gesamt.(under_after(j)).(budget_for(i)).LossRxnNum(m))));
                   else 
                       gesamt.(under_after(j)).(budget_for(i)).Loss(m,k)= 0;
                   end
               end
           end

    end
end

end

%%
% Add RRG and Reactionformular to gesamt.xxx.xxx.Prod/Loss
for j=1:length(under_after)
    for i=1:length(budget_for)
        gesamt.(under_after(j)).(budget_for(i)).Loss(:,end+2)=sum(gesamt.(under_after(j)).(budget_for(i)).Loss,2);
        gesamt.(under_after(j)).(budget_for(i)).Loss = array2table(gesamt.(under_after(j)).(budget_for(i)).Loss);
        gesamt.(under_after(j)).(budget_for(i)).Loss=addvars(gesamt.(under_after(j)).(budget_for(i)).Loss,gesamt.(under_after(j)).(budget_for(i)).LossRxn);
        gesamt.(under_after(j)).(budget_for(i)).Loss=addvars(gesamt.(under_after(j)).(budget_for(i)).Loss,gesamt.(under_after(j)).(budget_for(i)).LossRxnNum);
        gesamt.(under_after(j)).(budget_for(i)).Loss=sortrows(gesamt.(under_after(j)).(budget_for(i)).Loss,(width(gesamt.(under_after(j)).(budget_for(i)).Loss)-2),'ascend');         
        gesamt.(under_after(j)).(budget_for(i)).Prod(:,end+2)=sum(gesamt.(under_after(j)).(budget_for(i)).Prod,2);
        gesamt.(under_after(j)).(budget_for(i)).Prod = array2table(gesamt.(under_after(j)).(budget_for(i)).Prod);
        gesamt.(under_after(j)).(budget_for(i)).Prod=addvars(gesamt.(under_after(j)).(budget_for(i)).Prod,gesamt.(under_after(j)).(budget_for(i)).ProdRxn);
        gesamt.(under_after(j)).(budget_for(i)).Prod=addvars(gesamt.(under_after(j)).(budget_for(i)).Prod,gesamt.(under_after(j)).(budget_for(i)).ProdRxnNum);
        gesamt.(under_after(j)).(budget_for(i)).Prod=sortrows(gesamt.(under_after(j)).(budget_for(i)).Prod,(width(gesamt.(under_after(j)).(budget_for(i)).Prod)-2),'descend');
    end
end


%% For the close all

for i=1:length(budget_for)
  for j=1:length (under_after)

        Prod_plot1 = table2array(gesamt.(under_after(j)).(budget_for(i)).Prod(:,1:(width(gesamt.(under_after(j)).(budget_for(i)).Prod)-4)));
        Loss_plot1 = table2array(gesamt.(under_after(j)).(budget_for(i)).Loss(:,1:(width(gesamt.(under_after(j)).(budget_for(i)).Loss)-4)));
        max_legend_entries = 4;
                if height(Prod_plot1)>(max_legend_entries)
                    Prod_plot = Prod_plot1(1:(max_legend_entries),:);
                    Prod_others = sum(Prod_plot1((max_legend_entries+1):end,:),1);
                    Prod_plot = [Prod_plot;Prod_others];
                    Legend_prod = [table2array(gesamt.(under_after(j)).(budget_for(i)).Prod(1:(max_legend_entries),end-1));'Others'];
                else
                     Prod_plot = Prod_plot1(1:end,:);
                     Legend_prod = table2array(gesamt.(under_after(j)).(budget_for(i)).Prod(1:end,end-1));              
                end
                
                if height(Loss_plot1)>(max_legend_entries)
                    Loss_plot = Loss_plot1(1:(max_legend_entries),:);
                    Loss_others = sum(Loss_plot1((max_legend_entries+1):end,:),1);
                    Loss_plot = [Loss_plot;Loss_others];
                    Legend_Loss = [table2array(gesamt.(under_after(j)).(budget_for(i)).Loss(1:(max_legend_entries),end-1));'Others'];    
                else
                     Loss_plot = Loss_plot1(1:end,:);
                     Legend_Loss = table2array(gesamt.(under_after(j)).(budget_for(i)).Loss(1:end,end-1));              
                end
Loss_plot = Loss_plot.';
Prod_plot = Prod_plot.';

if under_after(j)=="intern"
    figure;
    subplot(2,1,1);
    title(strcat("Production of " , budget_for(i) ," ",(under_after(j))))
    hold on; grid on;
    bar((NOmr(2:end)), Prod_plot(2:end,:),'stacked')
    legend(Legend_prod,'Location','northeast','FontSize',9)%(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat(("NO [mr]"))); ylabel(strcat(budget_for(i), " [mr/s]"));
    xlim([0 2E-3])
    
    subplot(2,1,2);
    hold on; grid on;
    title(strcat("Loss of " , budget_for(i) ," ",(under_after(j))))
    bar((NOmr(2:end)), Loss_plot (2:end,:) ,'stacked')
    legend(Legend_Loss,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat(" [mr]"));ylabel(strcat(budget_for(i), " [+/-mr]"));
    
    if save_figures == "Yes"
        filename = strcat(budget_for(i),"_budget_",(under_after(j)));
        saveas(gca, fullfile(fname, VOC,"Budget", filename), 'fig');
    end
    
else
     
    figure;
    subplot(2,1,1);
    title(strcat("Production of " , budget_for(i) ," ",(under_after(j))," lamp"))
    hold on; grid on;
    bar(VOCmr(2:end), Prod_plot(2:end,:),'stacked','width',1.2)
    legend(Legend_prod,'Location','northeast','FontSize',9)%(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat((VOC(v))," [mr]"));ylabel(strcat(budget_for(i), " [+/-mr]"));

    subplot(2,1,2);
    hold on; grid on;
    title(strcat("Loss of " , budget_for(i) ," ",(under_after(j))," lamp"))
    bar(VOCmr(2:end), Loss_plot(2:end,:) ,'stacked')
    legend(Legend_Loss,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
    xlabel(strcat((VOC(v))," [mr]"));ylabel(strcat(budget_for(i), " [+/-mr]"));
    
        if save_figures == "Yes"
        filename = strcat(budget_for(i),"_budget_",(under_after(j)));
        saveas(gca, fullfile(Budget_folder, filename), 'fig');
        end
    end
                
                
                
                
                
  end
end
% Jetzt kommt der cut für nur 4 in legende + others --> summe vom Rest

%% Hartwig
% gesamt.Prod=NaN(length(gesamt.ProdRxnNum))

 %% in Loss all loss data per timestep same for prod
% prod = struct; loss = struct;
% 
% for l=1:length(budget_for)
%     for m=1:length (under_after)
%         
%         % looks into budgets and picks the smallest number of Prod and Loss
%         % reactions (we will loose reactions with E-30 or so)
%         [x,vertical_fieldnum]=size(budgets.(under_after(m)).(budget_for(l)));
%         A =[];C=[];
%         for j=2:vertical_fieldnum
%             [a,b]=size(budgets.(under_after(m)).(budget_for(l))(j).Prod);
%             A=[A,a];
%             [c,b]=size(budgets.(under_after(m)).(budget_for(l))(j).Loss);
%             C=[C,c];
%         end
%         manual_end_prod =  min(A);
%         manual_end_loss =  min(C);
%         
%         clear A a b C c x vertical_fieldnum
%         
%         
%                if under_after(m)=="intern"
%                 length_loop = length(NOmr);
%                else
%                 length_loop = length(VOCmr);
%                end
%             
%             for k= 2:length_loop
%                 prod1=flip(budgets.(under_after(m)).(budget_for(l))(k).Prod);
%                 prod_cut(:,k-1) = flip(prod1(1:manual_end_prod));
%                 legendp1=flip(budgets.(under_after(m)).(budget_for(l))(k).ProdRxn);
%                 legendprod_cut(:,k-1) = flip(legendp1(1:manual_end_prod));
%                 loss1=flip(budgets.(under_after(m)).(budget_for(l))(k).Loss);
%                 loss_cut(:,k-1) = flip(loss1(1:manual_end_loss));  
%                 legendl1=flip(budgets.(under_after(m)).(budget_for(l))(k).LossRxn)
%                 legendloss_cut(:,k-1) = flip(legendl1(1:manual_end_loss));
%             end
%             
%         prod_cut = prod_cut.';prod.(under_after(m)).(budget_for(l)) = prod_cut;
%         loss_cut = loss_cut.';loss.(under_after(m)).(budget_for(l)) = loss_cut;
%         prod1=[]; loss1=[]; loss_cut=[]; prod_cut =[];legendp1=[];legendl1=[],legendprod_cut={},legendloss_cut={};
%     end
% end
% 
% clear loss0 loss1 loss2 loss3 prod0 prod1 prod2 prod3 loss_cut
% %% Plot of Loss and Prod
% for l=1:length(budget_for)
%   for m=1:length (under_after)
%       for k=1:length(budgets.(under_after(m)).(budget_for(l)))
%       legend_max = 4;
%       A = (flip(budgets.(under_after(m)).(budget_for(l))(k).ProdRxn));
%       legend_prod.(under_after(m)).(budget_for(l)) = flip(A(1:4,1));
%       end
%   end
% end
% 
% 
% for l=1:length(budget_for)
%   for m=1:length (under_after)
%       %to reduce the number of plottet and showed reactions to max 4
%       legend_max = 4;
%      [a,b]=size(prod.(under_after(m)).(budget_for(l)));
%      [c,d]= size(loss.(under_after(m)).(budget_for(l)));
%      %for prod
%      if b>=5
%          prod_plotend=(prod.(under_after(m)).(budget_for(l))(:,end-(legend_max-1):end));
%          prod_plot1=sum(prod.(under_after(m)).(budget_for(l))(:,1:end-(legend_max)),2);
%          prod_plot =[prod_plot1,prod_plotend];
%          %legend
%          legend_prod_display = budgets.(under_after(m)).(budget_for(l))(4).ProdRxn(end-(legend_max):end);
%          legend_prod_display(1)={'Others'};
%      else
%          prod_plot=prod.(under_after(m)).(budget_for(l));
%          legend_prod_display = budgets.(under_after(m)).(budget_for(l))(4).ProdRxn;
%      end
%      
%      %for loss
%      if d>=5
%          loss_plotend=(loss.(under_after(m)).(budget_for(l))(:,end-(legend_max-1):end));
%          loss_plot1=sum(loss.(under_after(m)).(budget_for(l))(:,1:end-(legend_max)),2);
%          loss_plot =[loss_plot1,loss_plotend];
%          %legend
%          legend_loss_display = budgets.(under_after(m)).(budget_for(l))(4).LossRxn(end-(legend_max):end);
%          legend_loss_display(1)={'Others'};
%      else
%          loss_plot = loss.(under_after(m)).(budget_for(l));
%          legend_loss_display = budgets.(under_after(m)).(budget_for(l))(4).LossRxn;
%      end
% % if under_after(m)=="intern"
% %     
% %     figure;
% %     subplot(2,1,1);
% %     title(strcat("Production of " , budget_for(l) ," ",(under_after(m))))
% %     hold on; grid on;
% %     bar(log10(NOmr(2:end)), prod_plot,'stacked')
% %     legend(legend_prod,'Location','northeast','FontSize',9)%(4) without a special reason (in hope at every time the same reactions)
% %     xlabel(strcat(("log_1_0 NO [mr]"))); ylabel(strcat(budget_for(l), " [mr/s]"));
% % 
% %     subplot(2,1,2);
% %     hold on; grid on;
% %     title(strcat("Loss of " , budget_for(l) ," ",(under_after(m))))
% %     bar(log10(NOmr(2:end)), loss_plot ,'stacked')
% %     legend(legend_loss,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
% %     xlabel(strcat(" log_1_0 NO [mr]"));ylabel(strcat(budget_for(l), " [+/-mr]"));
% %     
% %     if save_figures == "Yes"
% %         filename = strcat(budget_for(l),"_budget_",(under_after(m)));
% %         saveas(gca, fullfile(fname, VOC,"Budget", filename), 'fig');
% %     end
% %     
% % else
% %      
% %     figure;
% %     subplot(2,1,1);
% %     title(strcat("Production of " , budget_for(l) ," ",(under_after(m))," lamp"))
% %     hold on; grid on;
% %     bar(VOCmr(2:end), prod_plot,'stacked')
% %     legend(legend_prod,'Location','northeast','FontSize',9)%(4) without a special reason (in hope at every time the same reactions)
% %     xlabel(strcat((VOC(v))," [mr]"));ylabel(strcat(budget_for(l), " [+/-mr]"));
% % 
% %     subplot(2,1,2);
% %     hold on; grid on;
% %     title(strcat("Loss of " , budget_for(l) ," ",(under_after(m))," lamp"))
% %     bar(VOCmr(2:end), loss_plot ,'stacked')
% %     legend(legend_loss,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
% %     xlabel(strcat((VOC(v))," [mr]"));ylabel(strcat(budget_for(l), " [+/-mr]"));
% %     
% %         if save_figures == "Yes"
% %         filename = strcat(budget_for(l),"_budget_",(under_after(m)));
% %         saveas(gca, fullfile(Budget_folder, filename), 'fig');
% %         end
% %     end
%   end
% end
% hold off
% %% Plot Zero-Value OH without VOC
% %  for m=1:length (under_after)
% % figure;
% % subplot(2,1,1);
% % title(strcat("Production of OH without VOC"," ",(under_after(m))," lamp"));
% % hold on; grid on;
% % bar([1,2],prod.(under_after(m)).OHzeroVOC,'stacked')
% % legend(budgets.(under_after(m)).OH(1).ProdRxn,'Location','northeast','FontSize',7)%(4) without a special reason (in hope at every time the same reactions)
% % legend('toggle')
% % xlabel(strcat((VOC)," mr = 0"));ylabel(" OH ");
% % 
% % subplot(2,1,2);
% % hold on; grid on;
% % title(strcat("Loss of OH without VOC", " ",(under_after(m))," lamp") )
% % bar([1,2],loss.(under_after(m)).OHzeroVOC,'stacked')
% % legend(budgets.(under_after(m)).OH(1).LossRxn,'Location','southeast','FontSize',9) %(4) without a special reason (in hope at every time the same reactions)
% % legend('toggle')
% % xlabel(strcat((VOC)," mr = 0"));ylabel(" OH ");
% % hold off
% % 
% %  end
% end
