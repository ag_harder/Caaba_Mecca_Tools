% function []=merge_data_netcdf(index,time_data,HO2,RO2_unsaturated,RO2_saturated,Onl_p20,Onl_TempK_int,density,Vol_flow)

%% merge data and create netcdf file
clearvars;
Config_at_userpath;

load(fullfile(datapath,'processed_data','AQABA_199_243_wmr_Int_Fon_Plume_Fon.mat'));
load([modelpath,'RO2_conc.mat']);
load([modelpath,'RO2_conc_picarro_for_posters.mat']);
load([datapath,'weather\WeatherStation\DWD_Weather_station_1min.mat']);
load([modelpath,'test_input_clean.mat']);
load([datapath,'calibrations\NO_titrations\Vol_and_mass_flow.mat']);

date_correction=datenum('31.12.2016','dd.mm.yyyy');
%% data from specific time
Gas_constant=8.314;
Avogadro=6.022e23;

% time_index=find(Onl_Time>230.5+datenum('2017','yyyy') & Onl_Time<230.6+datenum('2017','yyyy') & InstrumentAction(onl)==3 );
% time_index=find(Onl_Time>216.38+datenum('2017','yyyy') & Onl_Time<216.4+datenum('2017','yyyy') & InstrumentAction(onl)==3 );
time_index=find(Onl_Time>225.38+date_correction & Onl_Time<225.4+date_correction & InstrumentAction(onl)==3 );
NO=[0.5;1;1.5;2;3.5;5;7;10;15;20;30;40;50;70;90;110;130;150];
Vol_flow=mean(Vol_flow_int(time_index));
press_int=mean(Onl_p20_ax3(time_index));
temp_int=mean(Onl_TempK_int_ax3(time_index));
air_density=mean(density(time_index));
%HO2_data=1.3734E9./air_density;%5.316E8./air_density;
HO2_data=2.342e+06./(air_density.*(press_int./1013));%[4.3734E9,10.316E8]./air_density;%5.316E8./air_density;
CH3O2=0;%5.933e+06./(air_density.*(press_int./1013));% mean(RO2_saturated(time_index))./air_density;
HOCH2CH2O2=0;%5.027e+05./(air_density.*(press_int./1013));% mean(RO2_unsaturated(time_index))./air_density;
% LISOPACO2=mean(RO2_saturated(time_index))./air_density;

length_array=length(NO);

%% input custom data
% Gas_constant=8.314;
% Avogadro=6.022e23;
% Loschmidt=2.686e19;
% length_array=8;%16;
% press_int=repmat(3,[1,length_array]);%linspace(2.5,4.0,length_array);
% temp_int=linspace(295,330,length_array);%repmat(295,[1,length(press_int)]);
% % [Vol_flow]=get_Vol_flow(temp_int,press_int.*100);
% Vol_flow=0.0397;
% NO=10;
% HO2_data=0;%5E8./Loschmidt;
% CH3O2=0;%5E8./Loschmidt;
% HOCH2CH2O2=0;%5E8./Loschmidt;

%% input test values
model_input.HO2=repmat(HO2_data,[1,length_array]);% repmat(HO2_data,[1,length(NO)]);
model_input.OH=zeros(1,length_array);
model_input.Tgps=1:length_array;
model_input.press=repmat(press_int.*100,[1,length_array]);
model_input.temp=repmat(temp_int,[1,length_array]);
% model_input.NO=repmat([NO_flow.*1e-6./60.*0.1./Vol_flow(temp_index)],[1,3]); % .*1e-6./60 from sccm/min to sL/s    .*0.1 bottle concentration 10%
model_input.NO=NO.*1e-6./60.*(1013./press_int).*0.1./Vol_flow;
model_input.CH4=zeros(1,length_array);
model_input.J_O1D=zeros(1,length_array);
model_input.J_NO2=zeros(1,length_array);
model_input.J_CH3O2=zeros(1,length_array);
model_input.J_CH3ONO=zeros(1,length_array);
model_input.J_HONO=zeros(1,length_array);
model_input.spechum=zeros(1,length_array);
model_input.CO=zeros(1,length_array);
model_input.NO2=zeros(1,length_array);
model_input.H2O2=zeros(1,length_array);
model_input.O3=zeros(1,length_array);
model_input.CH3O2=repmat(CH3O2,1,length(NO)); %#ok<REPMAT>
% model_input.C2H5O2=repmat(CH3O2,1,length_array);
model_input.HOCH2CH2O2=repmat(HOCH2CH2O2,1,length_array); %#ok<REPMAT>
% model_input.LISOPACO2=repmat(LISOPACO2,1,length_array);

%% delete negative data
temp_neg_data(length(model_input.Tgps))=false;

for k=1:length(model_input.Tgps)
    if  (model_input.Tgps(k)<0 || model_input.temp(k)<0 || model_input.HO2(k)<0 || model_input.press(k)<0)
        temp_neg_data(k)=true;
    end
end

model_input.Tgps(temp_neg_data)=[];
model_input.temp(temp_neg_data)=[];
model_input.HO2(temp_neg_data)=[];
model_input.press(temp_neg_data)=[];

varnames=fields(model_input);
% save([stargate_path,'input_mat\input_data_measured_Dan_NO_test.mat'],'model_input');
% save([stargate_path,'input_mat\input_measured_data\input_data_measured_',num2str(index),'.mat'],'model_input','reaction_time','k_HO2_NO');

%% Create netCDF file from MatLab dataset
fname    = 'NO_titration_fit_HOCH2CH2O2_correct_rate_constant_HO2_only.nc';

if exist([stargate_path,fname]) %#ok<EXIST>
    fclose('all');
    clear mex %#ok<CLMEX>
    delete([stargate_path,fname]);
end

ncid     = netcdf.create([stargate_path,fname],'NC_NOCLOBBER'); % Open netCDF file.
len_data = [size(model_input.Tgps,2),size(varnames,1)];   % Define the dimensions of the variable
%dim      = len_data(1)+1;
%dimid    = netcdf.defDim(ncid,'T_AX',len_data);
dimid    = netcdf.defDim(ncid,'time',netcdf.getConstant('NC_UNLIMITED'));

for i = 1 : len_data(2)                          % Define a new variable in the file.
    varID   = netcdf.defVar(ncid,varnames{i},'double',dimid);
    netcdf.endDef(ncid);                    % Leave define mode and enter data mode to write data.
    netcdf.putVar(ncid,varID,0,len_data(1), eval(['model_input.',varnames{i}]));      % Write data to variable.
    netcdf.reDef(ncid);                     % Leave data mode and re-enter define mode
    % netcdf.putAtt(ncid,varID,'units','mixing ratio'); % define attribute for missing value
end
netcdf.close(ncid);


