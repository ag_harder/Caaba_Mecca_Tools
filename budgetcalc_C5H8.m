function budget=budgetcalc_C5H8(DATFILE, MODELOUTSTRUCT)
% can be used to read output of getBudgets.tcsh (BudgetXY.dat) from caaba
% to a matlab structure and calculate Prod. and Loss rates

% read data from BudgetXY.dat
fid = fopen(DATFILE);
budget.data = textscan(fid, '%s %s %s %s','delimiter',';')
fclose(fid);

for i=1:size(budget.data{1,1},1)    
  if isfield(MODELOUTSTRUCT,['RR',budget.data{1,1}{i}]) 
   eval(['budget.data{1,5}(i,:)=MODELOUTSTRUCT.RR',budget.data{1,1}{i},'*str2num(budget.data{1,2}{i});'])
  else
    budget.data{1,5}(i,:)=zeros(size(MODELOUTSTRUCT.OH));
%     disp(['WARNING: RR',budget.data{1,1}{i},' could not be found in model output structure!']);
%     disp(['         Prod/Loss rate for ',budget.data{1,4}{i},' is set to ZERO!']);        % NaN doesn`t work because of sorting later...;-(
  end
end  
% add median column for sorting of importance
budget.data{1,5}(:,end+1)=nanmedian(budget.data{1,5},2);

% sort data to find loss and production quickly (takes first dataset as
% reference to find most important rates)
[budget.sortdata,sortind]=sortrows(budget.data{1,5},size(budget.data{1,5},2));

% filter each sorted matrix for sign (Prod="+",Loss="-")
ProdFilter=(nanmedian(budget.sortdata,2)>0);
LossFilter=(nanmedian(budget.sortdata,2)<0);

budget.Prod=budget.sortdata(ProdFilter,1:end-1);
budget.Loss=flipud(budget.sortdata(LossFilter,1:end-1));

% find indices to refer to proper equation and equation number...
ProdInd=sortind(ProdFilter);
LossInd=sortind(LossFilter);

budget.ProdRxnNum=budget.data{1,1}(ProdInd);
budget.ProdRxnPart=budget.data{1,3}(ProdInd);
budget.ProdRxn=budget.data{1,4}(ProdInd);

budget.LossRxnNum=flipud(budget.data{1,1}(LossInd));
budget.LossRxnPart=flipud(budget.data{1,3}(LossInd));
budget.LossRxn=flipud(budget.data{1,4}(LossInd));

% Total prod. and loss rates:
budget.TotalProd=sum(budget.Prod,1);
budget.TotalLoss=sum(budget.Loss,1);






