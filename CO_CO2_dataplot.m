clearvars
My_Config;
filename = strcat("CO_CO2_Run_CLOUD_Multirun_CO");

readin_model_output_runs(stargate_path,filename)  %Outcomment if matlabdata is already available
readin_model_output(stargate_path,filename)       %Outcomment if matlabdata is already available

load(fullfile(stargate_path,'output_matlab',strcat('output_',filename,'.mat')));
load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename,'.mat')));

plot_mixingratio = 1;
plot_paticle_dens = 0;
x_axis = "runtime";
y_axis = ["OH","HO2","H2O2","O3","CO","CO2","NO2","NO"];

%Standards
p0=101325 ; t0=298.15; kB=1.380649E-23; % Pa, K, J/K
lozahl=(p0/(t0*kB)/1000000); %1/cm^3

runs_path=fullfile(stargate_path,filename,'\runs\');
d=dir2(runs_path);

for l=1:length(d)
if l<10
    run = strcat('run000',num2str(l));
elseif l>=10
    run = strcat('run00',num2str(l));
elseif l>=100
    run = strcat('run0',num2str(l));
elseif l>=1000
    run = strcat('run',num2str(l));
end


particle_dens = (lozahl*(p0./[runs.run0001.press]).*([runs.run0001.temp]./t0)).';


figure
%PLot mixingratios
for k=1:length (y_axis)
    
    if plot_mixingratio == true
    y = [runs.(run).(y_axis(k))].';

    
    switch x_axis 
        case "runtime"
            x=[runs.(run).runtime].';x=x./60;  
            x_label = "[runtime min]";
    end


    subplot(3,3,k)
    grid on; hold on;
    title(y_axis(k))
    plot(x,y)
    xlabel(x_label); ylabel(strcat(y_axis(k), " [mixingratio]"))
%standard_plot_Felix
    end


    if plot_paticle_dens == true
        figure
        % plots Particledenses
        for k=1:length (y_axis)
            y = [runs.(run).(y_axis(k))].'.*particle_dens;


        switch x_axis 
            case "runtime"
                x=[runs.(run).runtime].';x=x./60;  
                x_label = "[runtime min]";
        end


        subplot(3,3,k)
        grid on; hold on;
        title(y_axis(k))
        plot(x,y)
        xlabel(x_label); ylabel(strcat(y_axis(k), " [particle dens]"))
        %standard_plot_Felix
        end
    end
end
end
% plot CO CO2 O3 OH
% wieviel Ozon müssen wir zugeben um 1E7 OH zu haben und reicht das für 30
% ppb CO2 in 1h 