
function [sum_RR_runs,sum_RR_output_struct] = sum_RR(stargate_path,timestep_under,timestep_after,timestep_intern,filename_under,filename_after,filename_intern)
%This function sums up the RR over the time of the WHOLE run
%It overwrites the RR values because in the normal output_struct
% are only the endvalues. The UNIT of RR is normally [1/s] 
% Output is how often the Reactions happened NO UNIT
% TIMESTEP [ms]!!!

% Loads structures for before and after lamp and combines them
output_struct = struct('under',[],'after',[]);
u = load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_under,'.mat')));
output_struct(1).under =u.output_struct;
u = load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_after,'.mat')));
output_struct.after =u.output_struct;
u = load(fullfile(stargate_path,'output_matlab',strcat('output_',filename_intern,'.mat')))
output_struct.intern =u.output_struct;

runs = struct('under',[],'after',[]);
u = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_under,'.mat')));
runs(1).under =u.runs;
u = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_after,'.mat')));
runs.after =u.runs;
u = load(fullfile(stargate_path,'output_matlab',strcat('output_runs_',filename_intern,'.mat')));
runs.intern =u.runs;
clear u 

%%

timestep = [timestep_under, timestep_after,timestep_intern];
after_under = ["under","after","intern"];
sum_RR_runs = struct;
for k=1:length(after_under)
    for l=1:length(fields(runs.(after_under(k))))
        if l<10
            run = strcat('run000',num2str(l));
        elseif l>=10
            run = strcat('run00',num2str(l));
        elseif l>=100
            run = strcat('run0',num2str(l));
        end
        fieldnames_run = fieldnames(runs.(after_under(k)).(run));
        fieldnames_RR = fieldnames_run(contains(fieldnames(runs.(after_under(k)).(run)),'RR'));
        T2=setdiff(fieldnames_run,fieldnames_RR);
        
        sum_run = sum(((table2array(struct2table(runs.(after_under(k)).(run)))/1000)*timestep(k)),1);
        
        sum_run = array2table(sum_run);
        sum_run.Properties.VariableNames= fieldnames_run;
        sum_run = removevars(sum_run,T2); %removes all fields without 'RR'
        
%         sum_run=setdiff(fieldnames_run,T2);
% T2 = removevars(T1,{'Loss','Customers'});
        %turns into struct
        sum_RR_runs.(after_under(k)).(run)=table2struct(sum_run);
        
        %Overwrites the RR-Values of the outputstruct
        for m=1:length(fieldnames_RR)
            output_struct.(after_under(k))(l).(char(fieldnames_RR(m)))=sum_RR_runs.(after_under(k)).(run).(char(fieldnames_RR(m)));
            end
    end
       
end

sum_RR_output_struct=output_struct;

end



