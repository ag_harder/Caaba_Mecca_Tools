NOmr = [no_H2O.output_struct.NO];
RO2_ratio = [1-[(no_H2O.output_struct(1:10).CH3O2)]./((no_H2O.output_struct(1).CH3O2))];
HO2_ratio = [1-[(no_H2O.output_struct(1:10).HO2)]./((no_H2O.output_struct(1).HO2))];
NOmr_log= log10(NOmr);

figure
plot (NOmr_log,RO2_ratio)
hold on; grid on;
plot (NOmr_log,HO2_ratio)
xlabel('logNO [mr]'); ylabel('Reduction Ratio');
legend('RO2','HO2')